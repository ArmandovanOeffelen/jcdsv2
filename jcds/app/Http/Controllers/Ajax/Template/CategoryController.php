<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Category;
use App\Content;
use App\SubCategory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;

class CategoryController extends Controller
{

    public function showContent(User $company)
    {
        $categories = Category::all()->where('user_id','=', $company->id);
        $subcategories = SubCategory::all()->where('user_id','=', $company->id);

        return view('admin.categories.templates.index',[
            'company' =>$company,
            'categories' => $categories,
            'subcategories' => $subcategories,
        ]);
    }

    public function deleteCategory(Request $request,User $company,Category $category)
    {

        $subCategories = SubCategory::all()->where('category_id','=',$category->id);

        foreach($subCategories as $subcats){
            $subcats->delete();
        }

        $category->delete();

        return app()->call('App\Http\Controllers\Ajax\Template\CategoryController@showContent',[
            'company' => $company
        ]);
    }
    public function deleteSubCategory(Request $request,User $company,$subCategory)
    {

        $cat = SubCategory::findOrFail($subCategory);
        $cat->delete();

        return app()->call('App\Http\Controllers\Ajax\Template\CategoryController@showContent',[
            'company' => $company
        ]);
    }

}
