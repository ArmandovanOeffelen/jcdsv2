<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Content;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function showCompany(User $company)
    {
        $contents = Content::where('user_id','=',$company->id)->where('completed','=','0')->paginate(15);


        return view('admin.content.templates.company_overview',[
            'contents' => $contents,
            'company' => $company
        ]);
    }

    public function deleteCompany(User $company)
    {
        $company->delete();

        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showAllCompanies');
    }

    public function showAllCompanies()
    {

        $company = User::where('type','=','default')->OrderBy('id','DESC')->paginate(10);

        $company->withPath('/admin/user-management/overview');
        return view('admin.company.templates.index',[
            'company' => $company
        ]);
    }
}
