<?php

namespace App\Http\Controllers\Ajax\Template;

use App\Content;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;

class ContentController extends Controller
{

    public function index()
    {

        $content = Content::where('user_id','=', Auth::user()->id)->paginate(15);

        return view('default.content.templates.index',[
            'content' => $content,
        ]);
    }

    public function delete(Request $request,User $company,Content $content)
    {

        $content->delete();

        session()->flash('succes','verwijderd');
        return app()->call('App\Http\Controllers\Ajax\Template\ContentController@index');
    }

    public function markCompleted(Request $request,User $company, Content $content)
    {
        $content->completed = 1;
        $content->update();

        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showCompany',[
            'company' => $company
        ]);
    }

    public function deleteContent(Request $request,User $company, Content $content)
    {

        $content->delete();

        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showCompany',[
            'company' => $company
        ]);
    }

    public function downloadImage(Request $request, User $company, Content $content)
    {

       return Storage::download("/content_images/",$content->image_name);
    }
}
