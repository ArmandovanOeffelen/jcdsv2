<?php

namespace App\Http\Controllers\Ajax\Template;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CreditController extends Controller
{
    public function addCredits(Request $request, User $company)
    {

        $validator = Validator::make($request->all(), [
            'hour' => 'required'
        ]);


        $hour = round($request->hour,'2');
        $subHour = round($request->subHours,'2');


        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('forms.company.add_credits',[
                ])->withErrors($validator)->render()
            ), 400);
        }
        $currentCredit = 0;
        if(!$company->credit == null) {

            $currentCredit = $company->credit;
        }


        $creditToAdd = $hour+$subHour;

        $totalCredits = $currentCredit + $creditToAdd;

        $company->credit = $totalCredits;
        $company->update();


        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showAllCompanies');
    }

    public function EditCredits(Request $request, User $company)
    {
        $validator = Validator::make($request->all(), [
            'hour' => 'required'
        ]);


        $hour = round($request->hour,'2');
        $subHour = round($request->subHours,'2');

        if ($validator->fails()) {
            return response()->json(array(
                'html' => view('forms.company.add_credits',[
                ])->withErrors($validator)->render()
            ), 400);
        }

        if($company->credit > 0) {
            $currentCredit = $company->credit;
        }

        $creditToAdd = $hour+$subHour;

        $totalCredits = $currentCredit-$creditToAdd;


        $company->credit = $totalCredits;
        $company->update();


        return app()->call('App\Http\Controllers\Ajax\Template\CompanyController@showAllCompanies');
    }
}
