<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function index(User $company)
    {
        $categories = Category::all()->where('user_id','=', $company->id);
        $subcategories = SubCategory::all()->where('user_id','=', $company->id);

        return view('admin.categories.index',[
            'company' =>$company,
            'categories' => $categories,
            'subcategories' => $subcategories,
        ]);
    }

    public function showForm(User $company){

        return view('forms.category.create',[
            'company' => $company,
        ]);
    }

    public function create(User $company, Request $request)
    {

        $categories = Category::all()->where('user_id','=', $company->id);
        $subcategories = SubCategory::all()->where('user_id','=', $company->id);

        $validator = Validator::make($request->all(),[
            'categoryname' => 'required|min:4|max:35',
            'parentcategory' => 'required',
        ]);

        if($validator->fails()){
            return  view('admin.categories.index',[
                'company' => $company,
                'categories' => $categories,
                'subcategories' => $subcategories,
            ])->withErrors($validator)->render();
        }
        if($request->parentcategory == 0){
            $category = Category::create([
                'user_id' => $company->id,
                'name' => $request->categoryname,
            ]);

        } else {
            $subCategory = SubCategory::create([
                'category_id' => $request->parentcategory,
                'name' => $request->categoryname
            ]);


        }




        return redirect()->action('CategoryController@index',
            [
                'company'=>$company,
                'categories' => $categories,
                'subcategories' => $subcategories,
            ]);
    }
}
