<?php

namespace App\Http\Controllers;

use App\Category;
use App\Content;
use App\User;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $company = User::where('type','=','default')->OrderBy('id','DESC')->paginate(10);

        $company->withPath('/admin/user-management/overview');
        return view('admin.company.index',[
            'company' => $company
        ]);
    }

    public function showForm()
    {

        return view('forms.company.create');
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'companyname' => 'required|min:4|max:35',
            'project' => 'required',
            'email' => 'required|email|max:255|unique:users,email',
            'project' => 'required|min:3|max:35',
            'password' => 'required|min:4|max:25|confirmed',
        ]);

        if ($validator->fails()) {
            return view('forms.company.create', [
            ])->withErrors($validator)->render();
        }


        if ($request->has('logo')) {
            $file = $request->file('logo');
            $imageName = $request->companyname . '-' . $request->project . '.' . $file->getClientOriginalExtension();

            $file->storeAs('/logo/', $imageName, 'public');

            $pathname = "storage/logo/";

            $user = User::create([
                'name' => $request->companyname,
                'project_name' => $request->project,
                'email' => $request->email,
                'logo_name' => $imageName,
                'logo_path' => $pathname,
                'credit' => 0.00,
                'type' => 'default',
                'password' => bcrypt($request->password)
            ]);


        } else {

            $user = User::create([
                'name' => $request->companyname,
                'project_name' => $request->project,
                'email' => $request->email,
                'credit' => 0.00,
                'type' => 'default',
                'password' => bcrypt($request->password)
            ]);
        }

        $standardCategory = new Category();
        $standardCategory->user_id = $user->id;
        $standardCategory->name = 'uncategorized';
        $standardCategory->is_deleteable = 0;
        $standardCategory->save();



        session()->flash('succes','Bedrijf is toegevoegd!');
        return redirect()->action('CompanyController@index');

    }

    public function update(Request $request, User $company)
    {

        if($request->password === null){
            $validator = Validator::make($request->all(),[
                'companyname' => 'required|min:4|max:35',
                'email' => 'required|email|unique:users,email,'.$company->id,
                'project' => 'required|min:3|max:35',
//            'password' => 'required|min:4|max:25|confirmed',
            ]);

            if($validator->fails()){
                return  view('forms.company.update',[
                    'company' => $company
                ])->withErrors($validator)->render();
            }

            $companyToUpdate = User::findOrFail($company->id);
            if($request->has('logo')){
                $file = $request->file('logo');
                $imageName = $request->companyname.'-'.$request->project.'.'.$file->getClientOriginalExtension();

                $file->storeAs('/logo/',$imageName,'public');

                $companyToUpdate->logo_name = $imageName;
            }



            $companyToUpdate->name = $request->companyname;
            $companyToUpdate->email = $request->email;
            $companyToUpdate->project_name = $request->project;

            $companyToUpdate->type = 'default';
            $companyToUpdate->update();
        } else {
            $validator = Validator::make($request->all(),[
                'companyname' => 'required|min:4|max:35',
                'email' => 'required|email|unique:users,email,'.$company->id,
                'project' => 'required|min:3|max:35',
                'password' => 'required|min:4|max:25|confirmed',
            ]);

            if($validator->fails()){
                return  view('forms.company.update',[
                    'company' => $company
                ])->withErrors($validator)->render();
            }

            $companyToUpdate = User::findOrFail($company->id);
            if($request->has('logo')){
                $file = $request->file('logo');
                $imageName = $request->companyname.'-'.$request->project.'.'.$file->getClientOriginalExtension();

                $file->storeAs('/logo/',$imageName,'public');

                $companyToUpdate->logo_name = $imageName;
            }

            $companyToUpdate->name = $request->companyname;
            $companyToUpdate->email = $request->email;
            $companyToUpdate->project_name = $request->project;
            $companyToUpdate->type = 'default';
            $companyToUpdate->password =  bcrypt($request->password);
            $companyToUpdate->update();
        }


        return app()->call('App\Http\Controllers\CompanyController@index');
    }

    public function showUpdateForm(User $company)
    {
        return view('forms.company.update',[
            'company' => $company
        ]);
    }

    public function delete(Request $request,User $company)
    {
        $companyToDelete = User::findOrFail($company->id);
        $companyToDelete->delete();

        return app()->call('App\Http\Controllers\CompanyController@index');
    }

    public function showCompanyCategories(User $company)
    {

    }

    public function showCompany(User $company)
    {
        $contents = Content::where('user_id','=',$company->id)->where('completed','=','0')->paginate(15);

        return view('admin.content.index',[
            'contents' => $contents,
            'company' => $company
        ]);
    }
}
