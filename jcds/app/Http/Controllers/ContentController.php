<?php

namespace App\Http\Controllers;

use App\Category;
use App\Content;
use App\Mail\ContentAdded;
use App\MailChecker;
use App\Traits\UploadTrait;
use App\User;
use Carbon\Carbon;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{

    use UploadTrait;

    public function index()
    {

        $content = Content::where('user_id','=', Auth::user()->id)->paginate(15);

        return view('default.content.index',[
            'content' => $content,
            ]);
    }

    public function showForm()
    {
        $company = Auth::user();
        $categories = Category::all()->where('user_id','=',$company->id);
        return view('forms.default.content.create',[
            'company' => $company,
            'categories' => $categories
        ]);
    }

    public function showUpdateForm(User $company,Content $content)
    {

        $categories = Category::all()->where('user_id','=',$company->id);
        return view('forms.default.content.update.index',[
            'company' => $company,
            'content' => $content,
            'categories' => $categories
        ]);
    }

    public function update(Request $request,User $company,Content $content)
    {
        if($request->hasFile('text_image')){
            $validator = Validator::make($request->all(),[
                'content_title' => 'required|min:4|max:35',
                'content_category' => 'required',
                'short_desc' => 'required',
                'long_desc' => 'required',
                'text_image' => 'required|image|mimes:jpeg,png,jpg',
            ]);

            $categories = Category::all()->where('user_id','=',$company->id);
            if($validator->fails()){
                return  view('forms.default.content.update.index',[
                    'company' => $company,
                    'categories' => $categories,
                    'content' => $content
                ])->withErrors($validator)->render();
            }

            $date = Carbon::now('Europe/Amsterdam')->format('d-m-yyy');

            $path = 'public/content_images';
            $file = $request->file('text_image');
            $imageName = $request->content_title.'-'.$date.'.'.$file->getClientOriginalExtension();
            $file->storeAs($path,$imageName);

            $pathname = "storage/content_images/".$imageName;

            $content->image_name = $imageName;
            $content->image_path = $pathname;

        } else {
            $validator = Validator::make($request->all(),[
                'content_title' => 'required|min:4|max:35',
                'content_category' => 'required',
                'short_desc' => 'required',
                'long_desc' => 'required',
            ]);

            if($validator->fails()){
                return  view('forms.default.content.update.index',[
                    'company' => $company,
                    'content' => $content
                ])->withErrors($validator)->render();
            }
        }

        $category_array = json_decode($request->content_category, true);
        $is_parent = $category_array['is_parent'] ?? null;

        //Parent Category Check
        if($is_parent != null){
            $parentCategoryId = $category_array['id'];
            $childCategoryId = null;
        } else {
            $childCategoryId = $category_array['id'];
            $parentCategoryId = $category_array['category_id'];
        }

        $content->user_id = $company->id;
        $content->category_id = $parentCategoryId;
        $content->sub_category_id = $childCategoryId;
        $content->content_title = $request->content_title;
        $content->short_desc = $request->short_desc;
        $content->long_desc = $request->long_desc;
        $content->button_link = $request->button_link;
        $content->button_text = $request->button_text;
        $content->completed = 0;
        $content->change_requested = 1;
        $content->update();

        return redirect('/content/overview');
    }


    public function create(Request $request, User $company)
    {


        //if content contains image, continue.
        if($request->hasFile('text_image')){
            $validator = Validator::make($request->all(),[
                'content_title' => 'required|min:4|max:35',
                'content_category' => 'required',
                'short_desc' => 'required',
                'long_desc' => 'required',
                'text_image' => 'required|image|mimes:jpeg,png,jpg',
            ]);

            $categories = Category::all()->where('user_id','=',$company->id);
            if($validator->fails()){
                $request->flash();
                return  view('forms.default.content.create',[
                    'categories' => $categories,
                    'company' => $company
                ])->withInput()->withErrors($validator)->render();
            }

            $date = Carbon::now('Europe/Amsterdam')->format('d-m-yyy');

            $path = 'public/content_images';
            $file = $request->file('text_image');
            $imageName = $request->content_title.'-'.$date.'.'.$file->getClientOriginalExtension();
            $file->storeAs($path,$imageName);

            $pathname = "storage/content_images/";


            $category_array = json_decode($request->content_category, true);
            $is_parent = $category_array['is_parent'] ?? null;

            //Parent Category Check
            if($is_parent != null){
                $parentCategoryId = $category_array['id'];
                $childCategoryId = null;
            } else {
                $childCategoryId = $category_array['id'];
                $parentCategoryId = $category_array['category_id'];
            }

            $content = new Content();
            $content->user_id = $company->id;
            $content->category_id = $parentCategoryId;
            $content->sub_category_id = $childCategoryId;
            $content->content_title = $request->content_title;
            $content->short_desc = $request->short_desc;
            $content->long_desc = $request->long_desc;
            $content->button_link = $request->button_link;
            $content->button_text = $request->button_text;
            $content->image_name = $imageName;
            $content->image_path = $pathname;
            $content->completed = 0;
            $content->change_requested = 0;
            $content->save();

        } else {
            //if content doesnt contain an image, continue.
            $validator = Validator::make($request->all(),[
                'content_title' => 'required|min:4|max:35',
                'content_category' => 'required',
                'short_desc' => 'required',
                'long_desc' => 'required',
            ]);

            $categories = Category::all()->where('user_id','=',$company->id);
            if($validator->fails()){
                $request->flash();
                return  view('forms.default.content.create',[
                    'categories' => $categories,
                    'company' => $company
                ])->withErrors($validator)->render();
            }

            $category_array = json_decode($request->content_category, true);
            $is_parent = $category_array['is_parent'] ?? null;

            //Parent Category Check
            if($is_parent != null){
                $parentCategoryId = $category_array['id'];
                $childCategoryId = null;
            } else {
                $childCategoryId = $category_array['id'];
                $parentCategoryId = $category_array['category_id'];
            }

            $content = new Content();
            $content->user_id = $company->id;
            $content->category_id = $parentCategoryId;
            $content->sub_category_id = $childCategoryId;
            $content->content_title = $request->content_title;
            $content->short_desc = $request->short_desc;
            $content->long_desc = $request->long_desc;
            $content->button_link = $request->button_link;
            $content->button_text = $request->button_text;
            $content->completed = 0;
            $content->change_requested = 0;
            $content->save();
        }


        $this->sendMail($company);

        return redirect('/content/overview')->with('succes','Content succesvol aangeleverd!');
    }

    public function sendMail(User $company)
    {
        $mail = new MailChecker();
        $mailAnswer = $mail->checkIfMailAlreadySendForCompany($company);
        if($mailAnswer == false ){

            $mail->is_send = 1;
            $mail->user_id = $company->id;
            $mail->save();

            $admin = User::where('type','=','admin')->first();
            return Mail::to($admin->email)->send(new ContentAdded($company,$admin));
        } else {

            return ;
        }


    }
    public function showCompanyContent(User $company,Content $content)
    {

        return view('admin.content.content_preview.index',[
            'content' => $content,
            'company' => $company
        ]);
    }
}
