<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use App\WorkFlow;
use App\WorkFlowProduct;
use App\WorkFlowStep;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WorkFlowController extends Controller
{
    public function index()
    {
        $workflows = WorkFlow::where('user_id', '=', Auth::user()->id)->withCount('workFlowStep')->simplePaginate(10);

        return view('default.workflow.index', [
            'workflows' => $workflows
        ]);
    }

    public function showForm()
    {
        $company = Auth::user()->id;
        return view('forms.default.workflow.index', [
            'company' => $company
        ]);
    }

    public function createMainWorkflow(Request $request, $company)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|min:4|max:35',
            'short_desc' => 'required',
        ]);

        if ($validator->fails()) {
            return view('forms.default.content.update.index')->withErrors($validator)->render();
        }

        $generalWorkFlow = new WorkFlow();
        $generalWorkFlow->user_id = $company;
        $generalWorkFlow->name = $request->title;
        $generalWorkFlow->description = $request->short_desc;
        $generalWorkFlow->status = 0;
        $generalWorkFlow->save();


        return response()->redirectToAction('WorkFlowController@showWorkFlow', [
            Auth::user()->id,
            $generalWorkFlow
        ]);
    }

    public function showStepForm($company, WorkFlow $workFlow)
    {
        $steps = WorkFlowStep::where('work_flow_id', '=', $workFlow->id)->get();
        $countAmount = count($steps);


        return view('forms.default.workflow.workflow-step.index', [
            'company' => $company,
            'workflow' => $workFlow,
            'countAmount' => $countAmount
        ]);
    }

    public function createWorkFlowStep(Request $request, $company, WorkFlow $workFlow)
    {

        //check if WorkFlow is completed and uncomplete if so.
        if($workFlow->completed === 1){
            $workFlow->completed = 0;
            $workFlow->update();
        }

        $steps = WorkFlowStep::where('work_flow_id', '=', $workFlow->id)->get();
        $countAmount = count($steps);

        $workFlowStep = new WorkFlowStep();

        $validator = Validator::make($request->all(), [
            'title' => 'required|min:4|max:35',
            'step' => 'required|integer|min:1|max:500',
            'days' => 'required',
            'hours' => 'required',
            'minutes' => 'required',
            'select_type' => 'required',
            'description' => 'required',
        ]);


        if ($request->select_type == 'manual') {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:4|max:35',
                'step' => 'required|integer|min:1|max:500',
                'days' => 'required',
                'hours' => 'required',
                'minutes' => 'required',
                'select_type' => 'required',
                'description' => 'required',
                'manual_button' => 'required|min:2|max:25'
            ]);

            $workFlowStep->step_type = $request->select_type;

        } elseif ($request->select_type == 'questionaire') {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:4|max:35',
                'step' => 'required|integer|min:1|max:500',
                'days' => 'required',
                'hours' => 'required',
                'minutes' => 'required',
                'select_type' => 'required',
                'questionaire' => 'required',
                'description' => 'required',
            ]);


            if ($request->questionaire == 'else') {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|min:4|max:35',
                    'step' => 'required|integer|min:1|max:500',
                    'days' => 'required',
                    'hours' => 'required',
                    'minutes' => 'required',
                    'select_type' => 'required',
                    'questionaire_else' => 'required|min:3|max:30',
                    'description' => 'required',
                ]);
                $workFlowStep->questionaire = $request->questionaire_else;

            } else {
                $workFlowStep->questionaire = $request->questionaire;
            }

            $workFlowStep->step_type = $request->select_type;
        } elseif ($request->select_type == 'product') {


            $workFlowStep->step_type = $request->select_type;
            if ($request->hasFile('text_image')) {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|min:4|max:35',
                    'step' => 'required|integer|min:1|max:500',
                    'days' => 'required',
                    'hours' => 'required',
                    'minutes' => 'required',
                    'select_type' => 'required',
                    'product_title' => 'required|min:3|max:25',
                    'short_desc' => 'required|min:3|max:255',
                    'long_desc' => 'required|min:3|max:255',
                    'button_text' => 'required|min:3|max:25',
                    'button_link' => 'required|min:3|max:50',
                    'text_image' => 'required|mimes:jpg,jpeg,png,gif',
                    'description' => 'required',
                ]);


                $date = Carbon::now('Europe/Amsterdam')->format('d-m-yyy');

                $path = 'public/products';
                $file = $request->file('text_image');
                $imageName = $request->content_title . '-' . $date . '.' . $file->getClientOriginalExtension();
                $file->storeAs($path, $imageName);

                $pathname = "storage/products/" . $imageName;


                $workFlowProduct = new WorkFlowProduct();
                $workFlowProduct->title = $request->product_title;
                $workFlowProduct->image_name = $imageName;
                $workFlowProduct->image_path = $pathname;
                $workFlowProduct->description = $request->short_desc;
                $workFlowProduct->long_description = $request->long_desc;
                $workFlowProduct->btn_text = $request->button_text;
                $workFlowProduct->btn_link = $request->button_link;
                $workFlowProduct->save();

                //setting product id
                $workFlowStep->work_flow_product_id = $workFlowProduct->id;

            } else {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|min:4|max:35',
                    'step' => 'required|integer|min:1|max:500',
                    'days' => 'required',
                    'hours' => 'required',
                    'minutes' => 'required',
                    'select_type' => 'required',
                    'product_title' => 'required|min:3|max:25',
                    'short_desc' => 'required|min:3|max:255',
                    'long_desc' => 'required|min:3|max:255',
                    'button_text' => 'required|min:3|max:25',
                    'button_link' => 'required|min:3|max:50',
                    'description' => 'required',
                ]);

                $workFlowProduct = new WorkFlowProduct();
                $workFlowProduct->title = $request->product_title;
                $workFlowProduct->description = $request->short_desc;
                $workFlowProduct->long_description = $request->long_desc;
                $workFlowProduct->btn_text = $request->button_text;
                $workFlowProduct->btn_link = $request->button_link;
                $workFlowProduct->save();

                //setting product id
                $workFlowStep->work_flow_product_id = $workFlowProduct->id;
            }

        } //So far no data whatsoever?
        elseif ($request->select_type == 'proffesional') {

            $validator = Validator::make($request->all(), [
                'title' => 'required|min:4|max:35',
                'step' => 'required|integer|min:1|max:500',
                'days' => 'required',
                'hours' => 'required',
                'minutes' => 'required',
                'select_type' => 'required',
                'description' => 'required',
            ]);

            $workFlowStep->step_type = 'proffesional';
            $workFlowStep->professional = 1;
        }

        if ($validator->fails()) {
            $request->flash();
            return view('forms.default.workflow.workflow-step.index', [
                'company' => $company,
                'workflow' => $workFlow,
                'countAmount' => $countAmount
            ])->withErrors($validator)->render();
        }

        if ($request->days == 'Dagen') {
            $workFlowStep->activation_days = null;
        } else {
            $workFlowStep->activation_days = $request->days;
        }
        if ($request->hours == 'Uren') {
            $workFlowStep->activation_hours = null;
        } else {
            $workFlowStep->activation_hours = $request->hours;
        }
        if ($request->minutes == 'Minuten') {
            $workFlowStep->activation_minutes = null;
        } else {
            $workFlowStep->activation_minutes = $request->hours;
        }

        $workFlowStep->work_flow_id = $workFlow->id;
        $workFlowStep->title = $request->title;
        $workFlowStep->step = $request->step;
        $workFlowStep->description = $request->description;
        $workFlowStep->save();

        return response()->redirectToAction('WorkFlowController@showWorkFlow', [
            'workflow' => $workFlow,
            'company' => $company
        ]);


    }

    public function showUpdateForm($company, WorkFlow $workFlow)
    {

        return view('forms.default.workflow.update.index', [
            'workFlow' => $workFlow,
            'company' => $company
        ]);
    }

    public function showWorkFlow($company, WorkFlow $workFlow)
    {
        $workFlowSteps = WorkFlowStep::where('work_flow_id', '=', $workFlow->id)->orderBy('step', 'DESC')->simplePaginate(10);
        return view('default.workflow.workflow.index', [
            'company' => $company,
            'workflow' => $workFlow,
            'workFlowSteps' => $workFlowSteps
        ]);
    }

    public function updateStepForm(User $company, WorkFlow $workFlow, WorkFlowStep $workFlowStep)
    {

        return view('forms.default.update.workflow.workflow-step.index', [
            'company' => $company,
            'workflow' => $workFlow,
            'workFlowStep' => $workFlowStep
        ]);

    }

    public function updateStep(Request $request, User $company, WorkFlow $workFlow, WorkFlowStep $workFlowStep)
    {

        //check if WorkFlow is completed and uncomplete if so.
        if($workFlow->completed === 1){
            $workFlow = 0;
            $workFlow->update();
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required|min:4|max:35',
            'step' => "required|integer|min:1|max:500",
            'days' => 'required',
            'hours' => 'required',
            'minutes' => 'required',
            'select_type' => 'required',
            'description' => 'required',
        ]);

        //let's check if workflowstep has a product, if so.
        //We need to check if he wants to update
        if ($request->select_type !== 'product') {
            if ($workFlowStep->step_type === 'product') {
                if (!$workFlowStep->WorkFlowStepProduct === null) {
                    $workFlowStep->WorkFlowStepProduct->delete();
                    $workFlowStep->work_flow_product_id = null;
                }
            }
        } elseif ($request->select_type == 'product') {
            if ($workFlowStep->workFlowStepProduct === null) {
                //If the product has a product start checking if one exists
                $workFlowStep->step_type = $request->select_type;
                if ($request->hasFile('text_image')) {
                    $validator = Validator::make($request->all(), [
                        'title' => 'required|min:4|max:35',
                        'step' => "required|integer|min:1|max:500",
                        'days' => 'required',
                        'hours' => 'required',
                        'minutes' => 'required',
                        'select_type' => 'required',
                        'product_title' => 'required|min:3|max:25',
                        'short_desc' => 'required|min:3|max:255',
                        'long_desc' => 'required|min:3|max:255',
                        'button_text' => 'required|min:3|max:25',
                        'button_link' => 'required|min:3|max:50',
                        'text_image' => 'required|mimes:jpg,jpeg,png,gif',
                        'description' => 'required',
                    ]);

                    $date = Carbon::now('Europe/Amsterdam')->format('d-m-yyy');

                    $path = 'public/products';
                    $file = $request->file('text_image');
                    $imageName = $request->content_title . '-' . $date . '.' . $file->getClientOriginalExtension();
                    $file->storeAs($path, $imageName);

                    $pathname = "storage/products/" . $imageName;

                    $workFlowProduct = new WorkFlowProduct();
                    $workFlowProduct->title = $request->product_title;
                    $workFlowProduct->image_name = $imageName;
                    $workFlowProduct->image_path = $pathname;
                    $workFlowProduct->description = $request->short_desc;
                    $workFlowProduct->long_description = $request->long_desc;
                    $workFlowProduct->btn_text = $request->button_text;
                    $workFlowProduct->btn_link = $request->button_link;
                    $workFlowProduct->save();

                    //setting product id
                    $workFlowStep->work_flow_product_id = $workFlowProduct->id;
                } else {
                    $validator = Validator::make($request->all(), [
                        'title' => 'required|min:4|max:35',
                        'step' => "required|integer|min:1|max:500",
                        'days' => 'required',
                        'hours' => 'required',
                        'minutes' => 'required',
                        'select_type' => 'required',
                        'product_title' => 'required|min:3|max:25',
                        'short_desc' => 'required|min:3|max:255',
                        'long_desc' => 'required|min:3|max:255',
                        'button_text' => 'required|min:3|max:25',
                        'button_link' => 'required|min:3|max:50',
                        'description' => 'required',
                    ]);

                    $workFlowProduct = new WorkFlowProduct();
                    $workFlowProduct->title = $request->product_title;
                    $workFlowProduct->description = $request->short_desc;
                    $workFlowProduct->long_description = $request->long_desc;
                    $workFlowProduct->btn_text = $request->button_text;
                    $workFlowProduct->btn_link = $request->button_link;
                    $workFlowProduct->save();

                    //setting product id
                    $workFlowStep->work_flow_product_id = $workFlowProduct->id;
                }
            }
        }

        if ($request->select_type == 'manual') {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:4|max:35',
                'step' => "required|integer|min:1|max:500",
                'days' => 'required',
                'hours' => 'required',
                'minutes' => 'required',
                'select_type' => 'required',
                'description' => 'required',
                'manual_button' => 'required|min:2|max:25'
            ]);

            $workFlowStep->step_type = $request->select_type;

        } elseif ($request->select_type === 'questionaire') {
            $validator = Validator::make($request->all(), [
                'title' => 'required|min:4|max:35',
                'step' => "required|integer|min:1|max:500",
                'days' => 'required',
                'hours' => 'required',
                'minutes' => 'required',
                'select_type' => 'required',
                'questionaire' => 'required',
                'description' => 'required',
            ]);

            if ($request->questionaire === 'else') {
                $validator = Validator::make($request->all(), [
                    'title' => 'required|min:4|max:35',
                    'step' => "required|integer|min:1|max:500",
                    'days' => 'required',
                    'hours' => 'required',
                    'minutes' => 'required',
                    'select_type' => 'required',
                    'questionaire_else' => 'required|min:3|max:30',
                    'description' => 'required',
                ]);

                $workFlowStep->questionaire = $request->questionaire_else;

            } else {
                $workFlowStep->questionaire = $request->questionaire;
            }

            $workFlowStep->step_type = $request->select_type;
        } elseif ($request->select_type == 'product') {


        } //So far no data whatsoever?
        elseif ($request->select_type == 'proffesional') {

            $validator = Validator::make($request->all(), [
                'title' => 'required|min:4|max:35',
                'step' => "required|integer|min:1|max:500",
                'days' => 'required',
                'hours' => 'required',
                'minutes' => 'required',
                'select_type' => 'required',
                'description' => 'required',
            ]);
            $workFlowStep->proffesional = 1;

        }

        if ($validator->fails()) {
            $request->flash();
            return view('forms.default.update.workflow.workflow-step.index', [
                'company' => $company,
                'workflow' => $workFlow,
                'workFlowStep' => $workFlowStep
            ])->withErrors($validator)->render();
        }

        if ($request->days == 'Dagen') {
            $workFlowStep->activation_days = null;
        } else {
            $workFlowStep->activation_days = $request->days;
        }
        if ($request->hours == 'Uren') {
            $workFlowStep->activation_hours = null;
        } else {
            $workFlowStep->activation_hours = $request->hours;
        }
        if ($request->minutes == 'Minuten') {
            $workFlowStep->activation_minutes = null;
        } else {
            $workFlowStep->activation_minutes = $request->hours;
        }

        $workFlowStep->step_type = $request->select_type;
        $workFlowStep->work_flow_id = $workFlow->id;
        $workFlowStep->title = $request->title;
        $workFlowStep->step = $request->step;
        $workFlowStep->description = $request->description;
        $workFlowStep->update();


        return response()->redirectToAction('WorkFlowController@showWorkFlow', [
            'workflow' => $workFlow,
            'company' => $company
        ]);
    }

    public function updateFlow(Request $request, $company, WorkFlow $workFlow)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:4|max:35',
            'short_desc' => 'required|min:4|max:500',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return view('forms.default.workflow.update.index', [
                'company' => $company,
                'workFlow' => $workFlow,
            ])->withErrors($validator)->render();
        }

        $workFlow->name = $request->title;
        $workFlow->description = $request->short_desc;
        $workFlow->update();

         return response()->redirectToAction('WorkFlowController@index', [
             'company' => $company
         ]);
    }


    public function showAdminContent($company)
    {

        $workflows = WorkFlow::where('user_id',$company)->withCount('WorkFlowStep')->simplePaginate(10);

        return view('admin.workflow.index',[
            'company' => $company,
            'workflows' => $workflows
        ]);
    }

    public function showWorkFlowSteps($company,WorkFlow $workFlow)
    {
        $workFlowSteps = WorkFlowStep::where('work_flow_id',$workFlow->id)->simplePaginate(10);
        return view('admin.workflow.step.index',[
            'workFlowSteps' => $workFlowSteps,
            'workFlow' => $workFlow,
            'company' => $company
        ]);
    }

    public function showWorkFlowStepContent($company,WorkFlow $workFlow,WorkFlowStep $workFlowStep)
    {


        return view('admin.workflow.step.step-content.index',[
            'workFlowStep' => $workFlowStep,
            'workFlow' => $workFlow,
            'company' => $company

        ]);
    }
}
