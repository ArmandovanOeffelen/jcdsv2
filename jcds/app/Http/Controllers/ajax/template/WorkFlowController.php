<?php

namespace App\Http\Controllers\Ajax\Template;

use App\User;
use App\WorkFlow;
use App\WorkFlowStep;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class WorkFlowController extends Controller
{
    public function delete(User $company, WorkFlow $workFlow, WorkFlowStep $workFlowStep)
    {

        $workFlowStep->delete();

        return response()->redirectToAction('WorkFlowController@showWorkFlowContent',[
            'workflow' => $workFlow,
            'company' => $company
        ]);
    }

    //This is for default users.
    public function showWorkFlowContent($company, WorkFlow $workFlow)
    {


        $workFlowSteps = WorkFlowStep::where('work_flow_id', '=', $workFlow->id)->orderBy('step', 'DESC')->simplePaginate(10);
        return view('default.workflow.workflow.Template.index', [
            'company' => $company,
            'workflow' => $workFlow,
            'workFlowSteps' => $workFlowSteps
        ]);
    }


    public function showAllWorkFlows($company)
    {

        $workflows = WorkFlow::where('user_id', '=', $company->id)->simplePaginate(10);

        return view('default.workflow.Template.index', [
            'workflows' => $workflows
        ]);
    }


    public function deleteWorkFlow($company, WorkFlow $workFlow)
    {
        //Deleting al foreignkeys aswell.
        foreach($workFlow->workFlowStep as $step){
            if ($step->WorkFlowStepProduct !== null) {
                $step->WorkFlowStepProduct->delete();
            }
            $step->delete();
        }

        $workFlow->delete();


        $workflows = WorkFlow::where('user_id', '=', Auth::user()->id)->simplePaginate(10);

        return view('default.workflow.template.index', [
            'workflows' => $workflows,
            'company' => $company
        ]);
    }

    public function markStepComplete(WorkFlow $workFlow,WorkFlowStep $workFlowStep)
    {
        $workFlowStep->completed = 1 ;
        $workFlowStep->update();

        $workFlowSteps = WorkFlowStep::where('work_flow_id',$workFlow->id)->simplePaginate(10);
        return view('admin.workflow.step.Template.index',[
            'workFlowSteps' => $workFlowSteps,
            'workFlow' => $workFlow,
            'company' => $workFlow->user_id
        ]);
    }

    public function showWorkFlowForAdmin($company,$workFlow)
    {

        $workFlowSteps = WorkFlowStep::where('work_flow_id',$workFlow->id)->simplePaginate(10);
        return view('admin.workflow.step.Template.index',[
            'workFlowSteps' => $workFlowSteps,
            'workFlow' => $workFlow,
            'company' => $company
        ]);
    }


    public function markWorkFlowComplete($company, WorkFlow $workFlow)
    {


        $workFlowStepsNotCompletedCount = WorkFlowStep::where('work_flow_id',$workFlow->id)->where('completed',0)->get()->count();
        if(!$workFlowStepsNotCompletedCount === 0){

            session()->flash('warning','Workflow is niet voltooid. Er zijn een aantal stappen die nog niet voltooid zijn. Voltooi deze eerst voordat u verder gaat.');
            //todo:: set view.
            return view('admin.workflow.Template.index');
        }


        $workFlow->completed = 1;
        $workFlow->update();

        $workflows = WorkFlow::where('user_id',$company)->withCount('WorkFlowStep')->simplePaginate(10);

        return view('admin.workflow.Template.index',[
            'company' => $company,
            'workflows' => $workflows
        ]);
    }
}
