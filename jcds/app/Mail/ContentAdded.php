<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContentAdded extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $admin;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company,$admin)
    {
        $this->company  =   $company;
        $this->admin    =   $admin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('programmamakersjohan@gmail.com','JCDS')
            ->subject("Er is nieuwe content toegevoegd door:".$this->company->name)
            ->view('mail.new_content',[
                'company' => $this->company
            ]);
    }
}
