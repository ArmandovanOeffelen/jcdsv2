<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name','user_id'
    ];

    public function User(){
        return $this->belongsTo(User::class);
    }

    public function subCategory()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function content()
    {
        return $this->hasMany(Content::class);
    }
}
