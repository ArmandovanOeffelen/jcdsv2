<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class MailChecker extends Model
{
    protected $table = "mail_send";


    public function checkIfMailAlreadySendForCompany(User $company)
    {
        $mailSend = MailChecker::where('user_id','=',$company->id)->whereDate('created_at', Carbon::today())->get();

        if(count($mailSend) > 0){

            return true;
        } else {

            return false;
        }
    }
}
