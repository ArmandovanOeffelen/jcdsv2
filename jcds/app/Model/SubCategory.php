<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_category';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name','category_id'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function content()
    {
        return $this->hasMany(Content::class);
    }
}
