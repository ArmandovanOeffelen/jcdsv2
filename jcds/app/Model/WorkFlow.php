<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkFlow extends Model
{
    protected $table = 'work_flow';

    public function workFlowStep()
    {
        return $this->hasMany(WorkFlowStep::class);
    }

}
