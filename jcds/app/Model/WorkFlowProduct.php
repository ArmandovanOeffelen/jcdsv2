<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkFlowProduct extends Model
{
    protected $table = 'work_flow_product';

    protected $fillable = ['title','description','long_description','image_name','image_path','btn_link','btn_text'];

    use SoftDeletes;

    public function WorkFlowStep()
    {
        return $this->hasMany(WorkFlowStep::class);
    }
}
