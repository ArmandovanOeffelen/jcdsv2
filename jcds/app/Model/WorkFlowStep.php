<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkFlowStep extends Model
{
    protected $table = 'work_flow_step';


    public function WorkFlowStepProduct()
    {
        return $this->belongsTo(WorkFlowProduct::class,'work_flow_product_id');
    }
}
