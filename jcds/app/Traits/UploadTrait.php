<?php
/**
 * Created by PhpStorm.
 * User: Armando
 * Date: 5/22/2019
 * Time: 12:00 PM
 */

namespace App\Traits;


trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : str_random(25);

        $file = $uploadedFile->storeAs($folder, $name.'.'.$uploadedFile->getClientOriginalExtension(), $disk);

        return $file;
    }
}