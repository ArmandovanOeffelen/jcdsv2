<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsinged()->references('id')->on('users');
                $table->integer('category_id')->unsinged()->references('id')->on('category');
                $table->integer('sub_category_id')->nullable()->unsinged()->references('id')->on('sub_category');
                $table->string('content_title');
                $table->string('short_desc',255);
                $table->string('long_desc');
                $table->string('button_link')->nullable();
                $table->string('button_text')->nullable();
                $table->string('image_name')->nullable();
                $table->string('image_path')->nullable();
                $table->tinyInteger('completed')->default(0);
                $table->tinyInteger('change_requested')->default(0);
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
