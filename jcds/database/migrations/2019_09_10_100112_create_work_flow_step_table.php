<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkFlowStepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_flow_step', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('work_flow_id');
            $table->integer('activation_days')->nullable();
            $table->integer('activation_hours')->nullable();
            $table->integer('activation_minutes')->nullable();
            $table->integer('step');
            $table->string('title');
            $table->string('step_type');
            $table->text('description');
            $table->string('image')->nullable();
            $table->string('image_path')->nullable();

            //We make these all nullable() so that if for example manual is selected we save the button text there and see which column is not_null and define their actions.
            $table->unsignedInteger('work_flow_product_id')->nullable();
            $table->string('manual')->nullable();
            $table->tinyInteger('professional')->nullable();
            $table->string('questionaire')->nullable();

            $table->tinyInteger('completed')->default(0);
            $table->timestamps();

            $table->foreign('work_flow_product_id')->references('id')->on('work_flow_product');
            $table->foreign('work_flow_id')->references('id')->on('work_flow');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_flow_step');
    }
}
