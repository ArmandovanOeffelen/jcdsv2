<?php

use Illuminate\Database\Seeder;

class addAdminAcc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'developer',
            'email' => 'dev@rmnddesign.com',
            'type' => 'admin',
            'logo_name' => 'logoJohan.png',
            'password' => bcrypt('Hoihoihoi1'),

        ]);
        DB::table('users')->insert([
            'name' => 'test',
            'email' => 'test@test.com',
            'type' => 'default',
            'logo_name' => 'logoJohan.png',
            'password' => bcrypt('Hoihoihoi1'),
        ]);
    }
}
