/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 44);
/******/ })
/************************************************************************/
/******/ ({

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(45);


/***/ }),

/***/ 45:
/***/ (function(module, exports) {

var long_desc_editor_init = CKEDITOR.replace('long_desc', {
    removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' + 'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' + 'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' + 'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' + 'About,Smiley,Blockquote'
});
var short_desc_editor_init = CKEDITOR.replace('short_desc', {
    removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' + 'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' + 'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' + 'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' + 'About,Smiley,Blockquote'
});

CKEDITOR.instances.short_desc.on('change', function () {
    var inputShortDesc = short_desc_editor_init.getData();
    var previewShortDesc = $('#live_preview #preview_shortdesc');

    previewShortDesc.html(inputShortDesc);
});

CKEDITOR.instances.long_desc.on('change', function () {
    var inputLongDesc = long_desc_editor_init.getData();
    var previewShortDesc = $('#live_preview #preview_longdesc');

    previewShortDesc.html(inputLongDesc);
});

//get form inputs
var inputTitle = $('.form-content #content_title').find('input');
var inputButton = $('.form-content #content_button_text').find('input');
var inputSelectCategory = $('.form-content #content_category_select').find('select');
var inputButtonLink = $('.form-content #content_button_link').find('input');

inputTitle.change(function () {
    var inputTitleVal = inputTitle.val();
    var previewTitle = $('#live_preview #preview_title');
    previewTitle.text(inputTitleVal);
});

inputButton.change(function () {
    var inputButtonVal = inputButton.val();
    var previewButton = $('#live_preview #preview_button');
    previewButton.text(inputButtonVal);
});

inputSelectCategory.change(function () {
    var inputSelectCategoryText = inputSelectCategory.find('option:selected').text();
    var previewCategory = $('#preview_category');
    previewCategory.text(inputSelectCategoryText);
});

inputButtonLink.change(function () {
    var inputButtonLinkVal = inputButtonLink.val();
    var previewButtonLink = $('#live_preview #preview_link');
    previewButtonLink.text(inputButtonLinkVal);
    console.log("test");
});

var inputFile = $('.form-content #content_image').find('input:file');
var previewImage = $('#preview_img');

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            previewImage.attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

inputFile.change(function () {
    readURL(this);
});

/***/ })

/******/ });