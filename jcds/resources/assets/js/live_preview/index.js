let long_desc_editor_init = CKEDITOR.replace( 'long_desc',{
    removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' +
    'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' +
    'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' +
    'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' +
    'About,Smiley,Blockquote'
});
let short_desc_editor_init = CKEDITOR.replace( 'short_desc',{
    removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' +
    'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' +
    'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' +
    'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' +
    'About,Smiley,Blockquote'
});


CKEDITOR.instances.short_desc.on('change',function () {
    let inputShortDesc = short_desc_editor_init.getData();
    let previewShortDesc = $('#live_preview #preview_shortdesc');

    previewShortDesc.html(inputShortDesc);
});

CKEDITOR.instances.long_desc.on('change',function () {
    let inputLongDesc = long_desc_editor_init.getData();
    let previewShortDesc = $('#live_preview #preview_longdesc');

    previewShortDesc.html(inputLongDesc);
});

//get form inputs
let inputTitle = $('.form-content #content_title').find('input');
let inputButton = $('.form-content #content_button_text').find('input');
let inputSelectCategory = $('.form-content #content_category_select').find('select');
let inputButtonLink = $('.form-content #content_button_link').find('input');

inputTitle.change(function () {
   let inputTitleVal = inputTitle.val();
    let previewTitle = $('#live_preview #preview_title');
    previewTitle.text(inputTitleVal);
});

inputButton.change(function () {
    let inputButtonVal = inputButton.val();
    let previewButton = $('#live_preview #preview_button');
    previewButton.text(inputButtonVal);

});

inputSelectCategory.change(function () {
    let inputSelectCategoryText = inputSelectCategory.find('option:selected').text();
    let previewCategory = $('#preview_category');
    previewCategory.text(inputSelectCategoryText);

});

inputButtonLink.change(function () {
    let inputButtonLinkVal = inputButtonLink.val();
    let previewButtonLink = $('#live_preview #preview_link');
    previewButtonLink.text(inputButtonLinkVal);
    console.log("test");
});

let inputFile = $('.form-content #content_image').find('input:file');
let previewImage = $('#preview_img');

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            previewImage.attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

inputFile.change(function(){
    readURL(this);
});