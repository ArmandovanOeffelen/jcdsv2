@extends('layouts.app')

@section('title', 'Categorieën')

@section('content')
    @include('admin.categories.templates.index')
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze categorie wilt verwijderen?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')
            /* Delete  subcaategoryy*/
                .on('click', '#btn-delete-sub-category', function () {
                    var category = $(this).data('category');
                    var subcat = $(this).data('subcategory');
                    var company = $(this).data('company');

                    BootstrapDialog.show({
                        title: 'Verwijder categorie',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('admin/ajax/template/user-management/{0}/sub-category/{1}/delete')}}'.format(company.id,subcat.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                })
            /* Delete  */
                .on('click', '#btn-delete', function () {
                    var objectData1 = $(this).data('category');
                    var objectData = $(this).data('company');
                    BootstrapDialog.show({
                        title: 'Verwijder categorie',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('admin/ajax/template/user-management/{0}/category/{1}/delete')}}'.format(objectData.id,objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection