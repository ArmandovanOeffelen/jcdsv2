@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>Categorie overzicht - {{$company->name}}</h2>
        </div>
        <div class="col-lg-2">
            <div style="padding-top: 25px">
                <a href="{{url('/admin/user-management/overview')}}" class="btn btn-primary">Terug naar overzicht</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7 col-lg-7">
            <h3>Categorie toevoegen</h3>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    @include('forms.category.create')
                </div>
            </div>
        </div>
        <div class="col-md-5 col-lg-5">
            <h3>Huidige categorieën</h3>

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <ul>
                        @if(count($categories) == 0)
                            <li>Er zijn nog geen categorieën toegevoegd.</li>
                        @else
                            @foreach($categories as $parentCategories)
                                <li>
                                    @if($parentCategories->is_deleteable == 1)
                                        {{$parentCategories->name}} - <button class="btn btn-danger btn-xs"  id="btn-delete" data-category="{{$parentCategories}}" data-company="{{$company}}">delete</button>
                                    @else
                                        {{$parentCategories->name}}
                                    @endif
                                    <ul>
                                        @foreach($parentCategories->subCategory as $subs)
                                            <li>{{$subs->name}} - <button class="btn btn-danger btn-xs"  id="btn-delete-sub-category" data-subcategory="{{$subs}}" data-company="{{$company}}">delete</button> </li>
                                        @endforeach
                                    </ul>
                                </li>


                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>