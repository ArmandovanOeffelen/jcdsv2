@extends('layouts.app')

@section('title', 'Alle bedrijven')

@section('content')
    @include('admin.company.templates.index')
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u dit bedrijf wilt verwijderen?</span>',
                add_credit: `@include('forms.company.add_credits')`,
                loading: '<span>Even geduld...</span>'
            };

            $('#content')
                .on('click', '#add_credits', function () {
                    var objectData1 = $(this).data('company');

                    BootstrapDialog.show({
                        title: 'Voeg credits toe',
                        message: templates.add_credit,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Add',
                            label: 'Voeg toe',
                            cssClass: 'btn-primary',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('admin/ajax/template/user-management/{0}/credits/add-credit')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        hour: dialog.getModalContent().find('select[name="hour"]').val(),
                                        subHours: dialog.getModalContent().find('select[name="subHour"]').val(),
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                })
                .on('click', '#delete_credits', function () {
                    var objectData1 = $(this).data('company');

                    BootstrapDialog.show({
                        title: 'Wijzig credit balans',
                        message: templates.add_credit,
                        nl2br: false,
                        buttons: [{
                            id: 'Cancel',
                            label: 'Annuleren',
                            cssClass: 'btn-danger',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Add',
                            label: 'Wijzig',
                            cssClass: 'btn-primary',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('admin/ajax/template/user-management/{0}/credits/edit-credit')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                        hour: dialog.getModalContent().find('select[name="hour"]').val(),
                                        subHours: dialog.getModalContent().find('select[name="subHour"]').val(),
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                })
            /* Delete organisation */
                .on('click', '#delete_company', function () {
                    var objectData1 = $(this).data('company');

                    BootstrapDialog.show({
                        title: 'Verwijder content',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('admin/ajax/template/user-management/{0}/delete')}}'.format(objectData1.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection