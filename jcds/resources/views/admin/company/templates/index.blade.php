@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>Bedrijven overzicht</h2>
        </div>
        <div class="col-lg-2 pull-right" style="padding-top:25px;">
            <a class="btn btn-primary" href="{{url('admin/user-management/add-company/form')}}"> Voeg bedrijf toe</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                    <tr>
                        <td>
                            #
                        </td>
                        <td>
                            Naam
                        </td>
                        <td>
                            Project naam
                        </td>
                        <td>
                            Email
                        </td>
                        <td>
                            Credits
                        </td>
                        <td>

                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($company as $index=>$companies)
                        <tr>
                            <td>{{ ($company->perPage() * ($company->currentPage() - 1)) + $index + 1 }}</td>
                            <td>{{$companies->name}}</td>
                            <td>{{$companies->project_name}}</td>
                            <td>{{$companies->email}}</td>
                            @if($companies->credit === null)
                                <td>0</td>
                            @else
                                <td>{{$companies->credit}}</td>
                            @endif
                            <td class="text-right">
                                <a href="{{url('/admin/user-management/'.$companies->id.'/content/overview')}}" class="btn btn-primary">Bekijk content</a>
                                <a href="{{route('admin_workflow_show_content',[$companies])}}" class="btn btn-primary">Bekijk workflow</a>
                                <a href="{{url('/admin/user-management/'.$companies->id.'/category/overview')}}" class="btn btn-primary">Beheer categorieën</a>
                                <a href="{{url('/admin/user-management/'.$companies->id.'/edit-company/show-form')}}" class="btn btn-primary">Bewerk gegevens</a>
                                <button id="add_credits" class="btn btn-primary" data-company="{{$companies}}" ><i class="fa fa-fw fa-plus"></i> Credits</button>
                                <button id="delete_credits" class="btn btn-primary" data-company="{{$companies}}" ><i class="fa fa-fw fa-minus"></i> Credits</button>
                                <button id="delete_company" class="btn btn-danger" data-company="{{$companies}}"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                {!! $company->links() !!}
            </div>
        </div>
    </div>
</div>