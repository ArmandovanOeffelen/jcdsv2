@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>{{$company->name}} - content overzicht</h2>
        </div>
        <div class="col-lg-2 pull-right" style="padding-top:25px;">
            <a class="btn btn-primary" href="{{url('admin/user-management/'.$company->id.'/content/overview')}}"> Terug naar overzicht</a>
        </div>
    </div>
    <div class="col-md-12 col-lg-12 col-xl-12">
        <div class="text-center">
            @include('admin.content.content_preview.templates.live_preview')
            <button id="btn-completed" class="btn btn-success" data-content="{{$content}}" data-company="{{$company}}">Markeer voltooid</button>
        </div>
    </div>
</div>