<div class="row">
    <div class="col-md-12 col-lg-12">
        <img id="preview_img" style="height: 350px !important; width: 200px !important;" src="{{asset("$content->image_path$content->image_name")}}" />

    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12" style="padding-bottom: 25px;">
        <h2 id="preview_title">{{$content->content_title}}</h2>
        <small id="preview_category"><i>
                @if(is_null($content->sub_category_id))
                  Hoofd categorie:  {{$content->category->name}}
                @else
                    Hoofd categorie:  {{$content->category->name}}
                <br/>
                    Sub categorie:  {{$content->subCategory->name}}
                @endif
            </i>
            <br/>
        </small> - <small>{{Carbon\Carbon::now('Europe/Amsterdam')->format('d-M-Y')}}</small>
    </div>
</div>
<div class="row"></div>
<div class="row">
    <div id="preview_shortdesc" class="col-md-12 col-lg-12">
        <p id="text"  style=" padding-bottom:25px; pading-left:25px; padding-right: 25px;">
           {!! $content->short_desc !!}
        </p>
    </div>
</div>
<div class="row"></div>
<div id="button_preview" class="row" style="padding-bottom: 25px;">
    <button id="preview_button"  class="btn btn-success btn-block disabled" disabled>{{$content->button_text}}</button>
    <small>Button link: </small><small id="preview_link">{{$content->button_link}}</small> <br/>
    <small>Button text: </small><small id="preview_link">{{$content->button_text}}</small>

</div>
<div class="row">
    <div id="preview_longdesc"  class="col-md-12 col-lg-12">
        <p  style=" padding-top:25px; pading-left:25px; padding-right: 25px;">
            {!! $content->long_desc !!}
        </p>
    </div>
</div>
