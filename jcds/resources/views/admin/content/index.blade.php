@extends('layouts.app')

@section('title', 'Content overview')

@section('content')
    @include('admin.content.templates.company_overview')
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                complete: '<span>Weet u zeker dat u deze wilt markeren als voltooid?</span>',
                delete: '<span>Weet u zeker dat u deze content wilt verwijderen?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')

            /* Create */
            /* Delete organisation */
                .on('click', '#btn_completed', function () {
                    var objectData = $(this).data('content');
                    var objectData1 = $(this).data('company');

                    BootstrapDialog.show({
                        title: 'Voltooi content',
                        message: templates.complete,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'success',
                            label: 'Voltooi',
                            cssClass: 'btn-success',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'put',
                                    url: '{{ url('admin/ajax/template/user/{0}/content-management/content/{1}/mark-completed')}}'.format(objectData1.id,objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                })

                .on('click', '#btn_download_image', function () {
                    var objectData = $(this).data('content');
                    var objectData1 = $(this).data('company');
                    $.ajax({
                        type: 'post',
                        url: '{{ url('admin/ajax/template/user/{0}/content-management/content/{1}/download-image')}}'.format(objectData1.id,objectData.id),
                        data: {
                            _token: '{{ csrf_token() }}',
                        },
                        success: function (data) {
                            $('#content').html(data);
                        },
                        error: function (error) {
                            if (error.responseJSON.error != null) {
                                $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                            }
                        }
                    });
                })
                .on('click', '#btn_delete', function () {
                    var objectData = $(this).data('content');
                    var objectData1 = $(this).data('company');

                    BootstrapDialog.show({
                        title: 'Verwijder content',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Danger',
                            label: 'Verwijder',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('admin/ajax/template/user/{0}/content-management/content/{1}/delete')}}'.format(objectData1.id,objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection