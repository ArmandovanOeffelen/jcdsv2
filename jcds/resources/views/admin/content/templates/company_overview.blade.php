@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>{{$company->name}} - content overzicht</h2>
        </div>
        <div class="col-lg-2 pull-right" style="padding-top:25px;">
            <a class="btn btn-primary" href="{{url('admin/user-management/overview')}}"> Terug naar overzicht</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Title
                    </td>
                    <td>
                        Categorie
                    </td>
                    <td>
                        Aanpassing aangevraagd
                    </td>
                    <td>

                    </td>
                </tr>
                </thead>
                <tbody>
                @if(count($contents) == 0)
                    <tr><td>Er is nog geen content toegevoegd voor dit project.</td>
                    <td></td><td></td><td></td><td></td></tr>
                @else
                    @foreach($contents as $index=>$content)
                        <tr>
                            <td>{{ ($contents->perPage() * ($contents->currentPage() - 1)) + $index + 1 }}</td>
                            <td>{{$content->content_title}}</td>
                            <td>{{$content->category->name}}</td>
                            <td>{{$content->change_requested}}</td>
                            <td>
                                <a href="{{url('/admin/user/'.$company->id.'/content-management/content/'.$content->id.'/show')}}" class="btn btn-primary">Bekijk content</a>
                                <button id="btn_completed" class="btn btn-success" data-content="{{$content}}" data-company="{{$company}}">Markeer als voltooid</button>
                                {{--<button id="btn_download_image" class="btn btn-primary" data-content="{{$content}}" data-company="{{$company}}">Download afbeelding</button>--}}
                                <button id="btn_delete" class="btn btn-danger" data-content="{{$content}}" data-company="{{$company}}">Verwijder</button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="pull-right">
                {!! $contents->links() !!}
            </div>
        </div>
    </div>
</div>