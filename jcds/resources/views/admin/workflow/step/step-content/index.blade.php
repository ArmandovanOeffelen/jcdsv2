@extends('layouts.app')

@section('title', 'Workflow step')

@section('content')
    @include('admin.workflow.step.step-content.template.index')
@endsection