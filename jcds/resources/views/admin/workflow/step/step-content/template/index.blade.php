@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5 col-lg-5">
            <h3>Workflow step {{$workFlowStep->title}}</h3>
        </div>
        <div class="col-md-7 col-lg-7">
            <div class="text-right" style="padding-top: 25px;">
                <a href="{{route('admin_workflow_step_show_content',[$workFlow,$company])}}" class="btn btn-primary">Terug naar overzicht</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-lg-2"></div>
        <div class="col-md-8 col-lg-8">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <td width="30%">
                        </td>
                        <td width="70%">
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            Step title
                        </td>
                        <td>
                            {{$workFlowStep->title}}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Step description
                        </td>
                        <td>
                            {!! $workFlowStep->description !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Active after
                        </td>
                        <td>
                            <p>
                                @if(!is_null($workFlowStep->activation_days))
                                    {{"$workFlowStep->activation_days days"}}
                                @else
                                    0 days
                                @endif
                            </p>
                            <p>
                                @if(!is_null($workFlowStep->activation_hours))
                                    {{"$workFlowStep->activation_hours hours"}}
                                @else
                                    0 days
                                @endif
                            </p>
                            <p>
                                @if(!is_null($workFlowStep->activation_minutes))
                                    {{"$workFlowStep->activation_minutes minutes"}}
                                @else
                                    0 minutes
                                @endif
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>step type</b>
                        </td>
                        <td>
                            {{$workFlowStep->step_type}}
                        </td>
                    </tr>
                            @if($workFlowStep->step_type === 'questionaire')
                                <tr>
                                    <td>Questionaire</td>
                                    @if($workFlowStep->questionaire === "dix")
                                        <td>DIX</td>
                                    @elseif ($workFlowStep->questionaire === "mini_dix")
                                        <td>MINI-DIX</td>
                                    @elseif ($workFlowStep->questionaire === "healthy_scan")
                                        <td>Gezond leven scan</td>
                                    @else
                                        <td>{{$workFlowStep->questionaire}}</td>
                                    @endif

                                </tr>
                            @elseif ($workFlowStep->step_type === 'manual')
                                <tr>
                                    <td>Manual</td>
                                    <td>{{$workFlowStep->manual}}</td>
                                </tr>
                            @elseif ($workFlowStep->step_type === 'proffesional')
                                <p>Professional</p>
                            @elseif ($workFlowStep->step_type === 'product')
                                <tr>
                                    <td>
                                        <b>Product details</b>
                                    </td>
                                    <td></td>
                                </tr>
                                @if(!is_null($workFlowStep->work_flow_product_id))
                                    <tr>
                                        <td>
                                           Product title
                                        </td>
                                        <td>
                                            {!! $workFlowStep->WorkFlowStepProduct->title !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                           Product description
                                        </td>
                                        <td>
                                            {!! $workFlowStep->WorkFlowStepProduct->description !!}
                                        </td>
                                        <td>
                                    </tr>
                                    <tr>
                                        <td>
                                           Product long description
                                        </td>
                                        <td>
                                            {!! $workFlowStep->WorkFlowStepProduct->long_description !!}
                                        </td>
                                        <td>
                                    </tr>
                                    <tr>
                                        <td>
                                           Product image
                                        </td>
                                        <td>
                                            <img id="preview_img" style="height: 250px !important; width: 200px !important;" src="{{asset($workFlowStep->WorkFlowStepProduct->image_path)}}" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Button link
                                        </td>
                                        <td>
                                            {!! $workFlowStep->WorkFlowStepProduct->btn_link !!}
                                        </td>
                                        <td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Button text
                                        </td>
                                        <td>
                                            {!! $workFlowStep->WorkFlowStepProduct->btn_text !!}
                                        </td>
                                        <td>
                                    </tr>
                                @endif
                            @endif
                </tbody>
            </table>
            <div class="text-right">
                <a href="{{route('admin_workflow_step_show_content',[$company,$workFlow])}}" class="btn btn-primary">Terug naar overzicht</a>
            </div>
        </div>
        <div class="col-md-2 col-lg-2"></div>
    </div>
</div>