@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5 col-lg-5">
            <h2>Workflow step overzicht</h2>
        </div>
        <div class="col-md-7 col-lg-7">
            <div class="text-right" style="padding-top: 25px;">
                <a href="{{route('admin_workflow_show_content',[$company])}}" class="btn btn-primary">Terug naar overzicht</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Name
                    </td>
                    <td>
                        Description
                    </td>
                    <td>
                        Completed
                    </td>
                    <td>
                        Step type
                    </td>
                    <td>

                    </td>
                    <td>

                    </td>
                </tr>
                </thead>
                <tbody>
                @foreach($workFlowSteps as $index => $workFlowStep)
                    <tr>
                        <td>{{ ($workFlowSteps->perPage() * ($workFlowSteps->currentPage() - 1)) + $index + 1 }}</td>
                        <td>
                            {{$workFlowStep->title}}
                        </td>
                        <td>
                            {!! $workFlowStep->description !!}
                        </td>
                        <td>
                            @if($workFlowStep->completed === 0)
                                No
                            @else
                                Yes
                            @endif
                        </td>
                        <td>
                            @if($workFlowStep->step_type === 'questionaire')
                                <p>Questionaire</p>
                            @elseif ($workFlowStep->step_type === 'manual')
                                <p>Manual</p>
                            @elseif ($workFlowStep->step_type === 'proffesional')
                                <p>Professional</p>
                            @elseif ($workFlowStep->step_type === 'product')
                                <p>Product</p>
                            @endif
                        </td>
                        <td class="text-right">
                            <a href="{{route('admin_workflow_step-content_show_content',[$company,$workFlow,$workFlowStep])}}" class="btn btn-primary">Check step</a>
                            @if($workFlowStep->completed === 0)
                                <button id="btn_completed" data-workflow="{{$workFlow}}" data-workflowstep="{{$workFlowStep}}" data-company="{{$company}}" class="btn btn-success">Mark completed</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $workFlowSteps->links() !!}
            </div>
        </div>
    </div>
</div>