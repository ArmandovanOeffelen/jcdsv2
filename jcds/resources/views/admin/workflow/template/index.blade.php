@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>Workflow overzicht</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Name
                    </td>
                    <td>
                        Description
                    </td>
                    <td>
                       Completed
                    </td>
                    <td>
                        Total steps
                    </td>
                    <td>

                    </td>
                </tr>
                </thead>
                <tbody>
                @foreach($workflows as $index=>$workflow)
                    <tr>
                        <td>{{ ($workflows->perPage() * ($workflows->currentPage() - 1)) + $index + 1 }}</td>
                        <td>
                            {{$workflow->name}}
                        </td>
                        <td>
                            {{$workflow->description}}
                        </td>
                        <td>
                            @if($workflow->completed === 0)
                                No
                            @else
                                Yes
                            @endif
                        </td>
                        <td>
                            {{$workflow->work_flow_step_count}}
                        </td>
                        <td class="text-right">
                            <a href="{{route('admin_workflow_step_show_content',[$company,$workflow])}}" class="btn btn-primary">Check steps</a>
                            @if($workflow->completed === 0)
                                <button id="btn_completed" data-workflow="{{$workflow}}" data-company="{{$company}}" class="btn btn-success">Mark completed</button>
                            @else($workflow->completed === 1)
                                <button id="btn_completed" data-workflow="{{$workflow}}" data-company="{{$company}}" class="btn btn-success" disabled>Completed</button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                {!! $workflows->links() !!}
            </div>
        </div>
    </div>
</div>