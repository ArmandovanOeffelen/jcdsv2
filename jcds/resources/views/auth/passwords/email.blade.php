@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <p for="email" class="col-md-4 control-label">Vraag een nieuw wachtwoord aan bij de administrators van dit programma</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
