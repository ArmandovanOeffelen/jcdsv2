@extends('layouts.app')

@section('title', 'Content overview')

@section('content')
    @include('default.content.templates.index')
@endsection

@section('scripts')
    @parent
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze content wilt verwijderen?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')

            /* Create */
                /* Delete organisation */
                .on('click', '#delete_content', function () {
                    var objectData = $(this).data('content');
                    var objectData1 = $(this).data('company');

                    console.log(objectData1.id);
                    console.log("Data23",objectData.id);
                    BootstrapDialog.show({
                        title: 'Verwijder content',
                        message: templates.delete.format(objectData.name),
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('admin/ajax/template/delete/{0}/content/{1}')}}'.format(objectData1.id,objectData.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection