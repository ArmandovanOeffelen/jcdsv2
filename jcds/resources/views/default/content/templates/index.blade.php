@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>content overzicht</h2><br />
            <h4>U heeft momenteel <b><i>@if(Auth::user()->credit == null) 0 @else {{Auth::user()->credit}} @endif</i> </b> credits</h4>
        </div>
        <div class="col-lg-2 pull-right" style="padding-top:25px;">
            <a class="btn btn-primary" href="{{url('content/show-form')}}"> Voeg content toe</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Naam
                    </td>
                    <td>
                        Categorie
                    </td>
                    <td>
                        Voltooid
                    </td>
                    <td>

                    </td>
                </tr>
                </thead>
                <tbody>
                    @foreach($content as $index => $contents)
                        @if(count($content) == 0)
                            <tr><td>

                                </td></tr>
                        @else
                            <tr>
                                <td>{{ ($content->perPage() * ($content->currentPage() - 1)) + $index + 1 }}</td>
                                <td>{{$contents->content_title}}</td>
                                @if(is_null($contents->sub_category_id))
                                    <td>{{$contents->category->name}}</td>
                                @else
                                    <td>{{$contents->subCategory->name}}</td>
                                @endif

                                @if($contents->completed > 0)
                                    <td>Is toegevoegd.</td>
                                @else
                                    <td>Niet toegevoegd.</td>
                                @endif
                                <td>
                                    <a href="{{url('content/'.Auth::user()->id.'/show-update-form/'.$contents->id)}}" class="btn btn-primary">Wijzigen</a>
                                    <a href="#" id="delete_content" class="btn btn-danger" data-content="{{$contents}}" data-company="{{Auth::user()}}">Verwijderen</a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
            <div class="pull-right">
                {!! $content->links() !!}
            </div>
        </div>
    </div>
</div>