@extends('layouts.app')

@section('title', 'Workflow overview')

@section('content')
    @include('default.workflow.template.index')
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

    <script src="{{ asset('js/live_preview/index.js') }}"></script>
    <script>
        $(function () {

            var templates = {
                delete: '<span>Weet u zeker dat u deze workflow wilt verwijderen?</span>',
                loading: '<span>Even geduld...</span>'
            };

            $('#content')

            /* Create */
            /* Delete organisation */
                .on('click', '#btn_deleteFlow', function () {
                    var workflow = $(this).data('workflow');
                    var company = $(this).data('company');

                    BootstrapDialog.show({
                        title: 'Verwijder content',
                        message: templates.delete,
                        nl2br: false,
                        buttons: [{
                            label: 'Annuleren',
                            action: function action(dialogRef) {
                                dialogRef.close();
                            }
                        }, {
                            id: 'Delete',
                            label: 'Delete',
                            cssClass: 'btn-danger',
                            autospin: true,
                            action: function action(dialog) {
                                dialog.enableButtons(false);
                                dialog.setClosable(false);

                                $.ajax({
                                    type: 'delete',
                                    url: '{{ url('admin/ajax/template/company/{0}/workflow/{1}/delete')}}'.format(company.id,workflow.id),
                                    data: {
                                        _token: '{{ csrf_token() }}',
                                    },
                                    success: function success(data) {
                                        dialog.close();

                                        $('#content').html(data);
                                    },
                                    error: function error(error) {
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.getButton('Delete').stopSpin();

                                        if (error.responseJSON.error != null) {
                                            $('#errors').html(error.responseJSON.error.html ? error.responseJSON.error.html : '');
                                            dialog.close();
                                        }
                                    }
                                });
                            }
                        }]
                    });
                });
        });

        $(function () {

            // Extended disable function
            $.fn.extend({
                disable: function(state) {
                    return this.each(function() {
                        var $this = $(this);
                        if($this.is('input, button, textarea, select'))
                            this.disabled = state;
                        else
                            $this.toggleClass('disabled', state);
                    });
                }
            });

        });

        // String.format
        if (!String.prototype.format) {
            String.prototype.format = function() {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function(match, number) {
                    return typeof args[number] != 'undefined'
                        ? args[number]
                        : match
                        ;
                });
            };
        }
    </script>
@endsection