
@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>Workflow overzicht</h2><br />
            <h4>U heeft momenteel <b><i>@if(Auth::user()->credit == null) 0 @else {{Auth::user()->credit}} @endif</i> </b> credits</h4>
        </div>
        <div class="col-lg-2 pull-right" style="padding-top:25px;">
            <a class="btn btn-primary" href="{{route('default_show_work_flow_form')}}">Voeg workflow toe</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Naam
                    </td>
                    <td>
                        Aantal stappen
                    </td>
                    <td>

                    </td>
                </tr>
                </thead>
                <tbody>
                @if($workflows->isEmpty())
                    <tr class="warning">
                        <td>
                            <p> Er zijn geen nog geen workflows toegevoegd.</p>
                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                        <td>

                        </td>
                    </tr>
                @else
                    @foreach($workflows as $index => $flows)
                            <tr>
                                <td>{{ ($workflows->perPage() * ($workflows->currentPage() - 1)) + $index + 1 }}</td>
                                <td>{{$flows->name}}</td>
                                <td>{{$flows->work_flow_step_count}}</td>
                                <td class="text-right">
                                    <a href="{{route('default_workflow_show_form_update',[Auth::user(),$flows])}}" class="btn btn-primary">Wijzigen</a>
                                    <a href="{{route('default_show_work_flow',[Auth::user()->id,$flows])}}" class="btn btn-primary">Bekijken</a>
                                    <a href="#" id="btn_deleteFlow" class="btn btn-danger" data-workflow="{{$flows}}" data-company="{{Auth::user()}}">Verwijderen</a>
                                </td>
                            </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
            <div class="pull-right">
                                {!! $workflows->links() !!}
            </div>
        </div>
    </div>
</div>