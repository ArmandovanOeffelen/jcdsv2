@include('partials.flash-message')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8 col-md-8">
            <h2>Workflow - {{ $workflow->name }}</h2><br />
        </div>
        <div class="col-lg-4 col-md-4 text-right" style="padding-top:25px;">
            <a class="btn btn-primary" href="{{ action('WorkFlowController@showStepForm',[$company,$workflow]) }}">Voeg stap toe</a>
            <a class="btn btn-primary" href="{{ route('default_controller_workflow_index') }}">Terug naar overzicht</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <table class="table-striped table table-responsive">
                <thead>
                <tr>
                    <td>
                        #
                    </td>
                    <td>
                        Naam
                    </td>
                    <td>
                        positie
                    </td>
                    <td>
                        type
                    </td>
                    <td class="text-right">

                    </td>
                </tr>
                </thead>
                <tbody>
                @if($workFlowSteps->isEmpty())
                    <tr class="warning">
                        <td>{{$workflow->name}} heeft nog geen stappen.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @else
                    @foreach($workFlowSteps as $index => $step)

                        <tr>
                            <td>{{ ($workFlowSteps->perPage() * ($workFlowSteps->currentPage() - 1)) + $index + 1 }}</td>
                            <td>{{$step->title}}</td>
                            <td>{{$step->step}}</td>
                            <td>
                                @if($step->step_type === 'product')
                                    Product
                                @elseif($step->step_type === 'manual')
                                    Manueel
                                @elseif($step->step_type === 'proffesional')
                                    Professional
                                @elseif($step->step_type === 'questionaire')
                                    Questionaire
                                @endif
                            </td>
                            <td class="text-right">
                                <a href="{{route('default_show_update_step_form_show_form',['company' => Auth()->user(),'workflow' => $workflow,'workflowstep' => $step])}}" class="btn btn-primary">Wijzigen</a>
                                <button id="btn-deleteStep" class="btn btn-danger" data-step="{{$step}}" data-workflow=""{{$workflow}} data-company="{{Auth::user()}}">Verwijderen</button>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div class="pull-right">
                {!! $workFlowSteps->links() !!}
            </div>
        </div>
    </div>
</div>