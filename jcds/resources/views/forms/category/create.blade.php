<form action="{{action('CategoryController@create',[$company->id])}}" method="POST">
    <div class="row">
        <div class="col-md-8">
            <div class="form-group {{$errors->has('categoryname') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Categorie naam</span>
                    <input class="form-control" type="text" name="categoryname"/>
                </label>
                @if ($errors->has('categoryname'))
                    <span class="help-block">{{ $errors->first('categoryname') }}</span>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="form-group {{$errors->has('parentcategory') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Parent Category</span>
                    <select class="form-control" name="parentcategory">
                            <option value="0">Geen sub categorie</option>
                        @foreach($categories as $index=>$category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    <small>Selecteer hoofd categorie om een sub-categorie te maken</small>
                </label>
                @if ($errors->has('parentcategory'))
                    <span class="help-block">{{ $errors->first('parentcategory') }}</span>
                @endif
            </div>
            {!! csrf_field() !!}
        </div>
    </div>
    <div class="row">
        <div class="center-block">
            <button class="btn btn-primary" type="submit">Voeg toe</button>
        </div>
    </div>
</form>