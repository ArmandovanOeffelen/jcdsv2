<div class="row">
    <div class="col-md-6">
        <div class="form-group {{$errors->has('hour') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Uren</span>
            <select class="form-control" type="text" name="hour">
                @for ($i = 0; $i <= 10; $i++)
                    <option value="{{ $i }}">{{ $i }}</option>
                @endfor

                    @if ($errors->has('hour'))
                        <option value="{{ Request::input('hour') }}"> {{Request::input('hour') }}</option>
                    @endif
            </select>
            </label>
            @if ($errors->has('hour'))
                <span class="help-block"> {{ $errors->first('hour') }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group {{$errors->has('subHour') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Minuten</span>
                <select class="form-control" type="text" name="subHour">
                    <option value="0" selected>0</option>
                    <option value="0.25" >15</option>
                    <option value="0.50" >30</option>
                    <option value="0.75" >45</option>
                </select>
            </label>
            @if ($errors->has('subHour'))
                <span class="help-block">{{ $errors->first('subHour') }}</span>
            @endif
        </div>
    </div>
</div>
