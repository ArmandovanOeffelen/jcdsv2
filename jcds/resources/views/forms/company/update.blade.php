@extends('layouts.app')

@section('title', 'Add Company')

@section('content')
    @include('partials.flash-message')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h2>{{$company->name}} bewerken</h2>
            </div>
            <div class="col-md-2 pull-right" style="padding-top: 25px;">
                <a class="btn btn-primary" href="{{url('/admin/user-management/overview')}}"> Terug naar overview </a>
            </div>

        </div>
        <form action="{{ action('CompanyController@update',[$company->id]) }}" enctype="multipart/form-data" method="POST">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{$errors->has('companyname') ? 'has-error': '' }}">
                        <label class="control-label checkbox">
                            <span>Bedrijfs naam</span>
                            <input class="form-control" type="text" name="companyname" value="{{$request->companyname ?? $company->name}}"/>
                        </label>
                        @if ($errors->has('companyname'))
                            <span class="help-block">{{ $errors->first('companyname') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{$errors->has('project') ? 'has-error': '' }}">
                        <label class="control-label checkbox">
                            <span>Bedrijfs project naam</span>
                            <input class="form-control" type="text" name="project" value="{{$request->companyname ?? $company->project_name}}"/>
                        </label>
                        @if ($errors->has('project'))
                            <span class="help-block">{{ $errors->first('project') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{$errors->has('email') ? 'has-error': '' }}">
                        <label class="control-label checkbox">
                            <span>Email bedrijf</span>
                            <input class="form-control" type="text" name="email" value="{{$request->companyname ?? $company->email}}"/>
                        </label>
                        @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{$errors->has('logo') ? 'has-error': '' }}">
                        <label class="control-label checkbox">
                            <span>bedrijfs logo</span>
                            <input class="form-control" name="logo" type="file" value="{{ Request::file('logo') }}" style="padding-bottom: 5px;">
                        </label>
                        @if ($errors->has('logo'))
                            <span class="help-block">{{ $errors->first('logo') }}</span>
                        @endif
                    </div>
                </div>
                {!! csrf_field() !!}
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{$errors->has('password') ? 'has-error': '' }}">
                        <label class="control-label checkbox">
                            <span>Wachtwoord</span>
                            <input class="form-control" name="password" type="text" value="{{ Request::input('password') }}">
                            <small>Velden leeg laten als u het wachtwoord niet aanpassen wilt</small>
                        </label>
                        @if ($errors->has('password'))
                            <span class="help-block">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{$errors->has('password_confirmation') ? 'has-error': '' }}">
                        <label class="control-label checkbox">
                            <span>Herhaal wachtwoord</span>
                            <input class="form-control" name="password_confirmation" type="text" value="{{ Request::input('password_confirmation') }}">

                        </label>
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>
                </div>
            </div>
            {{--<div class="row">--}}
            {{--<div class="form-group {{$errors->has('logo') ? 'has-error': '' }}">--}}
            {{--<label class="control-label checkbox">--}}
            {{--<span>bedrijfs logo</span>--}}
            {{--<input class="form-control" name="logo" type="file" value="{{ Request::input('logo') }}" style="padding-bottom: 5px;">--}}
            {{--</label>--}}
            {{--@if ($errors->has('logo'))--}}
            {{--<span class="help-block">{{ $errors->first('logo') }}</span>--}}
            {{--@endif--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="row">
                <div class="pull-right">
                    <button class="btn btn-primary" type="submit">Voeg toe</button>
                </div>
            </div>
        </form>
    </div>
@endsection
