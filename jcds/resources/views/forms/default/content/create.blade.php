@extends('layouts.app')

@section('title', 'Deliver content')

@section('content')
    @include('partials.flash-message')
    @include('forms.default.content.templates.create_content_overview')
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

    <script src="{{ asset('js/live_preview/index.js') }}"></script>


@endsection