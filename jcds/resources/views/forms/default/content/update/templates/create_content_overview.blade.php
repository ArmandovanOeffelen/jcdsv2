<div class="container-fluid">
    <div class="row">
        <div class="col-lg-10">
            <h2>Content bewerken</h2>
        </div>
        <div class="col-md-2 col-lg-2 pull-right" style="padding-top: 25px;">
            <a class="btn btn-primary" href="{{url('/content/overview')}}"> Terug naar overzicht </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-4 col-xl-4">
            <h3>Content</h3>
            <div class="form-content">
                @include('forms.default.content.update.templates.create_template')
            </div>
        </div>
        <div class="col-md-1 col-lg-1 col-xl-1">
        </div>
        <div class="col-md-7 col-lg-7 col-xl-7">
            <div class="text-center">
                <h3>Live preview</h3>
                <div id="live_preview" class="content">
                    @include('forms.default.content.update.templates.live_preview')
                </div>
            </div>
        </div>
    </div>
</div>