<form autocomplete="off" action="{{ action('ContentController@update',[$company->id,$content->id]) }}" enctype="multipart/form-data" method="POST">
    <div class="row">
        <div id="content_title" class="form-group {{$errors->has('content_title') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Product titel</span>
                <input id="content_title" class="form-control" type="text" onchange="changeLivePreview()" name="content_title" value="{{Request::input('content_title') ?? $content->content_title}}"/>
            </label>
            @if ($errors->has('content_title'))
                <span class="help-block">{{ $errors->first('content_title') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="content_category_select" class="form-group {{$errors->has('short_desc') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Menu categorie</span>
                <select class="form-control" name="content_category" >

                    @foreach($categories as $parents)
                        @if(is_null($content->sub_category_id) && $content->category_id === $parents->id)
                            <option value="{{$parents}}" data-parents="{{$parents->id}}" selected>{{$parents->name}}</option>
                        @else
                            <option value="{{$parents}}" data-parents="{{$parents->id}}">{{$parents->name}}</option>
                        @endif
                        @foreach($parents->subCategory as $children)
                            @if($content->sub_category_id === $children->id)
                                <option value="{{$children}}" data-fk_parent ="{{$children->category_id}}" selected> &nbsp; - {{$children->name}}</option>
                            @else
                                <option value="{{$children}}" data-fk_parent ="{{$children->category_id}}"> &nbsp; - {{$children->name}}</option>
                            @endif
                        @endforeach
                    @endforeach
                </select>
            </label>
        </div>
    </div>
    <div class="row">
        <div id="short_desc_div" class="form-group {{$errors->has('short_desc') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Korte tekst</span>
                <textarea id="short_desc" class="form-control" type="text" name="short_desc">{{ $content->short_desc ?? ''}}</textarea>
            </label>
            @if ($errors->has('short_desc'))
                <span class="help-block">{{ $errors->first('short_desc') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="long_desc_div" class="form-group {{$errors->has('long_desc') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Lange tekst</span>
                <textarea id="long_desc" class="form-control"  type="text" name="long_desc">{{ $content->long_desc ?? ''}}</textarea>
            </label>
            @if ($errors->has('long_desc'))
                <span class="help-block">{{ $errors->first('long_desc') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="content_button_text" class="form-group {{$errors->has('button_text') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Knop text</span>
                <input class="form-control" type="text" value="{{ $content->button_text ?? ''}}" name="button_text"/>
            </label>
            @if ($errors->has('button_text'))
                <span class="help-block">{{ $errors->first('button_text') }}</span>
            @endif
        </div>
        {!! csrf_field() !!}
    </div>
    <div class="row">
        <div id="content_button_link" class="form-group {{$errors->has('button_link') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Knop link</span>
                <input class="form-control" type="text" name="button_link" value="{{ $content->button_link ?? ''}}"/>
            </label>
            @if ($errors->has('button_link'))
                <span class="help-block">{{ $errors->first('button_link') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="content_image" class="form-group {{$errors->has('text_image') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Afbeelding</span>
                <input class="form-control" name="text_image" type="file" value="{{ Request::input('text_image') }}" style="padding-bottom: 5px;">
            </label>
            @if ($errors->has('text_image'))
                <span class="help-block">{{ $errors->first('text_image') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="pull-right">
            <button class="btn btn-primary" type="submit">Voeg toe</button>
        </div>
    </div>
</form>
@section('scripts')
    @parent
    <script>


    </script>


@endsection