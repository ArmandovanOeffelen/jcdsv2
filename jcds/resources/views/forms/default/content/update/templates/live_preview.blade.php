<div class="row">
    <div class="col-md-12 col-lg-12">
        <img id="preview_img" style="height: 350px; width: 200px;" src="{{asset("$content->image_path$content->image_name")}}" />
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-lg-12" style="padding-bottom: 25px;">
        <h2 id="preview_title">{{$content->content_tile ?? "Content title"}}</h2>
        <small id="preview_category"><i></i></small> - <small>{{Carbon\Carbon::now('Europe/Amsterdam')->format('d-M-Y')}}</small>
    </div>
</div>
<div class="row"></div>
<div class="row">
    <div id="preview_shortdesc" class="col-md-12 col-lg-12">
        <p id="text"  style=" padding-bottom:25px; pading-left:25px; padding-right: 25px;">
            {!! old('short_desc') ?? $content->short_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate.
            Posuere morbi leo urna molestie at elementum. Ut aliquam purus sit amet. Duis ultricies lacus sed turpis tincidunt id aliquet.
            Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus.
            Nulla pellentesque dignissim enim sit amet venenatis.
            Laoreet id donec ultrices tincidunt arcu non sodales neque sodales.
            Sollicitudin nibh sit amet commodo nulla. Eget mauris pharetra et ultrices neque ornare aenean euismod.'!!}
        </p>
    </div>
</div>
<div class="row"></div>
<div id="button_preview" class="row" style="padding-bottom: 25px;">
    <button id="preview_button"  class="btn btn-success  btn-block disabled" disabled>{{old('button_text') ?? $content->button_text ?? "Klik hier"}}</button>
    <small>Button link: </small><small id="preview_link">{{ old('button_link') ?? $content->button_link ?? "www.voorbeeld.nl"}}</small>
</div>
<div class="row">
    <div id="preview_longdesc"  class="col-md-12 col-lg-12">
        <p  style=" padding-top:25px; pading-left:25px; padding-right: 25px;">
            {!! old('long_desc') ?? $content->long_desc ?? 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate.
            Posuere morbi leo urna molestie at elementum. Ut aliquam purus sit amet. Duis ultricies lacus sed turpis tincidunt id aliquet.
            Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus.
            Nulla pellentesque dignissim enim sit amet venenatis.
            Laoreet id donec ultrices tincidunt arcu non sodales neque sodales.
            Sollicitudin nibh sit amet commodo nulla. Eget mauris pharetra et ultrices neque ornare aenean euismod.
            <br />
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate.
            Posuere morbi leo urna molestie at elementum. Ut aliquam purus sit amet. Duis ultricies lacus sed turpis tincidunt id aliquet.
            Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus.
            Nulla pellentesque dignissim enim sit amet venenatis.
            Laoreet id donec ultrices tincidunt arcu non sodales neque sodales.
            Sollicitudin nibh sit amet commodo nulla. Eget mauris pharetra et ultrices neque ornare aenean euismod.'!!}
        </p>
    </div>
</div>
