@extends('layouts.app')

@section('title', 'Deliver content')

@section('content')
    @include('partials.flash-message')
    @include('forms.default.update.workflow.workflow-step.template.update')
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>

    <script>
        let removeButtons =  'Cut,Copy,Paste,Undo,Redo,Anchor,Link,Scayt,SelectAll,' +
            'Replace,Find,Form,Subscript,Strike,Underline,Superscript,RemoveFormat,CreateDiv,Unlink' +
            'Anchor,Image,Flash,Table,HortizontalRule,SpecialChar,PageBreak,Iframe,' +
            'InsertPre,BgColor,TextColor,Maximize,ShowBlocks,' +
            'About,Smiley,Blockquote';

        let desc_editor_init = CKEDITOR.replace( 'description',{
            removeButtons: removeButtons
        });

        document.addEventListener('DOMContentLoaded', function() {
            $select = $("#workflow .funkyradio input[name='select_type']:checked");

            if($('#workflow #template_destination .row').length > 0){
                $templateDestination.empty();
            }

            if($select.val() == 'manual'){
                let $template = $('#manual_template').html();
                var $row = $($template);
                $templateDestination.append($row);

            } else if($select.val() == 'questionaire'){
                console.log("questionaire found");

                let $template = $('#questionaire_template').html();
                var $row = $($template);
                $templateDestination.append($row);

                $questionaire_select = $("#workflow #template_destination #questionaire select[name='questionaire']");
                $questionaire_else = $("#workflow #template_destination #questionaire_else");

                console.log($questionaire_select.val());

                if($questionaire_select.val() === "else"){

                    if($questionaire_select.val() === "else"){
                        if($questionaire_else.css('display') === 'none'){
                            $questionaire_else.css("display","block");
                        }
                    } else {
                        $questionaire_else.css("display","none");
                    }
                }

            } else if($select.val() == 'proffesional') {

            }else if($select.val() == 'product') {

                let $template = $('#product_template').html();
                let $row = $($template);
                $templateDestination.append($row);


                let long_desc_editor_init = CKEDITOR.replace( 'long_desc',{
                    removeButtons: removeButtons
                });
                let short_desc_editor_init = CKEDITOR.replace( 'short_desc',{
                    removeButtons: removeButtons
                });


                CKEDITOR.instances.short_desc.on('change',function () {
                    let inputShortDesc = short_desc_editor_init.getData();
                    let previewShortDesc = $('#live_preview #preview_shortdesc');

                    previewShortDesc.html(inputShortDesc);
                });

                CKEDITOR.instances.long_desc.on('change',function () {
                    let inputLongDesc = long_desc_editor_init.getData();
                    let previewShortDesc = $('#live_preview #preview_longdesc');

                    previewShortDesc.html(inputLongDesc);
                });

                //get form inputs
                let inputTitle = $('.form-content #content_title').find('input');
                let inputButton = $('.form-content #content_button_text').find('input');
                let inputSelectCategory = $('.form-content #content_category_select').find('select');
                let inputButtonLink = $('.form-content #content_button_link').find('input');


                inputTitle.change(function () {
                    let inputTitleVal = inputTitle.val();
                    let previewTitle = $('#live_preview #preview_title');
                    previewTitle.text(inputTitleVal);
                });

                inputButton.change(function () {
                    let inputButtonVal = inputButton.val();
                    let previewButton = $('#live_preview #preview_button');
                    previewButton.text(inputButtonVal);

                });

                inputSelectCategory.change(function () {
                    let inputSelectCategoryText = inputSelectCategory.find('option:selected').text();
                    let previewCategory = $('#preview_category');
                    previewCategory.text(inputSelectCategoryText);

                });

                inputButtonLink.change(function () {
                    let inputButtonLinkVal = inputButtonLink.val();
                    let previewButtonLink = $('#live_preview #preview_link');
                    previewButtonLink.text(inputButtonLinkVal);
                    console.log("test");
                });

                let inputFile = $('.form-content #content_image').find('input:file');
                let previewImage = $('#preview_img');

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            previewImage.attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                inputFile.change(function(){
                    readURL(this);
                });


            }

        }, false);

        $templateDestination = $('#template_destination');


        $("#workflow .funkyradio input[name='select_type']").on('change',function () {
            $select = $("#workflow .funkyradio input[name='select_type']:checked");

            if($('#workflow #template_destination .row').length > 0){
                $templateDestination.empty();
            }

            if($select.val() == 'manual'){
                let $template = $('#manual_template').html();
                var $row = $($template);
                $templateDestination.append($row);

            } else if($select.val() == 'questionaire'){
                let $template = $('#questionaire_template').html();
                var $row = $($template);
                $templateDestination.append($row);

                $questionaire_select = $("#workflow #template_destination #questionaire select[name='questionaire']");
                $questionaire_else = $("#workflow #template_destination #questionaire_else");


                $questionaire_select.on('change',function () {

                    if($questionaire_select.val() === "else"){
                        if($questionaire_else.css('display') == 'none'){
                            $questionaire_else.css("display","block");
                        }
                    } else {
                        $questionaire_else.css("display","none");
                    }

                });


            } else if($select.val() == 'proffesional') {

            }else if($select.val() == 'product') {

                let $template = $('#product_template').html();
                let $row = $($template);
                $templateDestination.append($row);


                let long_desc_editor_init = CKEDITOR.replace( 'long_desc',{
                    removeButtons: removeButtons
                });
                let short_desc_editor_init = CKEDITOR.replace( 'short_desc',{
                    removeButtons: removeButtons
                });

                CKEDITOR.instances.short_desc.on('change',function () {
                    let inputShortDesc = short_desc_editor_init.getData();
                    let previewShortDesc = $('#live_preview #preview_shortdesc');

                    previewShortDesc.html(inputShortDesc);
                });

                CKEDITOR.instances.long_desc.on('change',function () {
                    let inputLongDesc = long_desc_editor_init.getData();
                    let previewShortDesc = $('#live_preview #preview_longdesc');

                    previewShortDesc.html(inputLongDesc);
                });

                //get form inputs
                let inputTitle = $('.form-content #content_title').find('input');
                let inputButton = $('.form-content #content_button_text').find('input');
                let inputSelectCategory = $('.form-content #content_category_select').find('select');
                let inputButtonLink = $('.form-content #content_button_link').find('input');


                inputTitle.change(function () {
                    let inputTitleVal = inputTitle.val();
                    let previewTitle = $('#live_preview #preview_title');
                    previewTitle.text(inputTitleVal);
                });

                inputButton.change(function () {
                    let inputButtonVal = inputButton.val();
                    let previewButton = $('#live_preview #preview_button');
                    previewButton.text(inputButtonVal);

                });

                inputSelectCategory.change(function () {
                    let inputSelectCategoryText = inputSelectCategory.find('option:selected').text();
                    let previewCategory = $('#preview_category');
                    previewCategory.text(inputSelectCategoryText);

                });

                inputButtonLink.change(function () {
                    let inputButtonLinkVal = inputButtonLink.val();
                    let previewButtonLink = $('#live_preview #preview_link');
                    previewButtonLink.text(inputButtonLinkVal);
                    console.log("test");
                });

                let inputFile = $('.form-content #content_image').find('input:file');
                let previewImage = $('#preview_img');

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            previewImage.attr('src', e.target.result);
                        }

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                inputFile.change(function(){
                    readURL(this);
                });


            }

        });



    </script>

@endsection