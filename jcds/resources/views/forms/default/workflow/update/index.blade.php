@extends('layouts.app')

@section('title', 'Deliver content')

@section('content')
    @include('partials.flash-message')
    @include('forms.default.workflow.update.template.update')
@endsection
@section('scripts')
    @parent
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>



@endsection