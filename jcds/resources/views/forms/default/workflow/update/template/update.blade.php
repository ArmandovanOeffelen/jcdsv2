<div class="row">
    <div class="col-lg-10">
        <h2>{{$workFlow->name ?? ''}} bewerken</h2>
    </div>
    <div class="col-md-2 col-lg-2 pull-right" style="padding-top: 25px;">
        <a class="btn btn-primary" href="{{route('default_controller_workflow_index')}}"> Terug naar overzicht </a>
    </div>
</div>
<form action="{{action('WorkFlowController@updateFlow',[$company,$workFlow])}}" enctype="multipart/form-data" method="POST">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div id="content_title" class="form-group {{$errors->has('title') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Workflow titel</span>
                    <input id="title" class="form-control" type="text" value="{{ old('title') ?? $workFlow->name ?? ''}}"
                           name="title" required="true" />
                </label>
                @if ($errors->has('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="form-group {{$errors->has('short_desc') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Korte tekst</span>
                    <textarea id="short_desc" class="form-control" name="short_desc">{!! old('short_desc') ?? $workFlow->description ?? "" !!}</textarea>
                </label>
                @if ($errors->has('short_desc'))
                    <span class="help-block">{{ $errors->first('short_desc') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="pull-right">
                <button class="btn btn-primary" type="submit">Bewerken</button>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</form>