    <div class="row">
        <div id="content_title" class="form-group {{$errors->has('product_title') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Product titel</span>
                <input id="content_title" class="form-control" type="text" value="{{ old('product_title') ?: ''}}"
                       name="product_title" />
            </label>
            @if ($errors->has('content_title'))
                <span class="help-block">{{ $errors->first('product_title') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="short_desc_div" class="form-group {{$errors->has('short_desc') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Korte tekst</span>
                <textarea id="short_desc" class="form-control" type="text" name="short_desc">{!! old('short_desc') ?? "" !!}</textarea>
            </label>
            @if ($errors->has('short_desc'))
                <span class="help-block">{{ $errors->first('short_desc') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="long_desc_div" class="form-group {{$errors->has('long_desc') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Lange tekst</span>
                <textarea id="long_desc" class="form-control"  type="text" name="long_desc">{!! old('long_desc') ?? "" !!}</textarea>
            </label>
            @if ($errors->has('long_desc'))
                <span class="help-block">{{ $errors->first('long_desc') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="content_button_text" class="form-group {{$errors->has('button_text') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Knop text</span>
                <input class="form-control" type="text"  name="button_text" value="{{ old('button_text') ?: '' }}" />
            </label>
            @if ($errors->has('button_text'))
                <span class="help-block">{{ $errors->first('button_text') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="content_button_link" class="form-group {{$errors->has('button_link') ? 'has-error': '' }}">
            <label class="control-label checkbox">
                <span>Knop link</span>
                <input class="form-control" type="text" name="button_link" value="{{ old('button_link') ?: ''}}"/>
            </label>
            @if ($errors->has('button_link'))
                <span class="help-block">{{ $errors->first('button_link') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div id="content_image" class="form-group {{!$errors->isEmpty() ? 'has-error' : '' }}">
            <label class="control-label checkbox">
                <span>Afbeelding</span>
                <input class="form-control" name="text_image" type="file" value="{{ old('text_image') }}" style="padding-bottom: 5px;">
            </label>
            @if (!$errors->isEmpty())
                <span class="help-block text-warning">Upload uw afbeelding a.u.b. opnieuw.</span>
            @endif
        </div>
    </div>