<div class="row">
    <div class="col-lg-10">
        <h2>Workflow</h2>
    </div>
    <div class="col-md-2 col-lg-2 pull-right" style="padding-top: 25px;">
        <a class="btn btn-primary" href="{{route('default_controller_workflow_index')}}"> Terug naar overzicht </a>
    </div>
</div>
<form id="workflow" action="{{ action('WorkFlowController@createWorkFlowStep',[$company,$workflow]) }}" enctype="multipart/form-data" method="POST">
    {!! csrf_field() !!}
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-3">
            <h4>Algemene informatie workflow stap</h4>
        </div>
        <div class="col-md-8"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-4">
            <div id="content_title" class="form-group {{$errors->has('title') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Workflow titel</span>
                    <input id="title" class="form-control" type="text" value="{{ old('title') ?: ''}}"
                           name="title"/>
                </label>
                @if ($errors->has('title'))
                    <span class="help-block">{{ $errors->first('title') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <div id="content_title" class="form-group {{$errors->has('step') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>stap #</span>
                    <input id="title" class="form-control" type="text" value="{{ old('step') ?: $countAmount +1 ?: ''}}"
                           name="step"/>
                </label>
                @if ($errors->has('step'))
                    <span class="help-block">{{ $errors->first('step') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div id="desc_div" class="form-group {{$errors->has('description') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Lange tekst</span>
                    <textarea id="description" class="form-control"  type="text" name="description">{!! old('description') ?? "" !!}</textarea>
                </label>
                @if ($errors->has('description'))
                    <span class="help-block">{{ $errors->first('description') }}</span>
                @endif
        </div>
        <div class="col-md-1"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5">
            <h4>Volgende stap laten zien na</h4>
            <small>
                Bij voltooien van de stap, wanneer mag de gebruiker naar de volgende stap?
                Tip: Selecteer overal 0 om de gebruiker gelijk de volgende stap te laten invullen
            </small>
        </div>
        <div class="col-md-5">
            <h4>Inhoud stap</h4>
            <small>
                Wat moet de gebruiker doen om de stap te voltooien
            </small>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row"></div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group {{$errors->has('days') ? 'has-error': '' }}">
                        <select class="form-control" name="days">
                                <option selected>Dagen</option>
                                @for($i =0; $i<=10;$i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{$errors->has('hours') ? 'has-error': '' }}">
                        <select class="form-control" name="hours">
                            <option selected>Uren</option>
                            @for($i =0; $i<24;$i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{$errors->has('minutes') ? 'has-error': '' }}">
                        <select class="form-control" name="minutes">
                            <option selected>Minuten</option>
                            @for($i =0; $i<=60;$i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="funkyradio">
                <div class="funkyradio-default">
                    <input type="radio" name="select_type" id="radio1" value="manual"{{ old('select_type')=="manual" ? 'checked='.'"checked"' : '' }} />
                    <label for="radio1">Door knop</label>
                </div>
                <div class="funkyradio-default">
                    <input type="radio" name="select_type" id="radio2" value="proffesional" {{ old('select_type')=="proffesional" ? 'checked='.'"checked"' : '' }} />
                    <label for="radio2">Proffesioneel</label>
                </div>
                <div class="funkyradio-default">
                    <input type="radio" name="select_type" id="radio3" value="product" {{ old('select_type')=="product" ? 'checked='.'"checked"' : '' }} />
                    <label for="radio3">Product</label>
                </div>
                <div class="funkyradio-default">
                    <input type="radio" name="select_type" id="radio4" value="questionaire" {{ old('select_type')=="questionaire" ? 'checked='.'"checked"' : '' }} />
                    <label for="radio4">Vragenlijst</label>
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div id="template_destination">

    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div class="pull-right">
                <button class="btn btn-primary" type="submit">Aanmaken</button>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</form>
<template id="manual_template">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h4 class="text-center">Vul knop tekst in</h4>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div id="manual_button" class="form-group {{$errors->has('manual_button') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Knop tekst</span>
                    <input id="title" class="form-control" type="text" value="{{ old('manual_button') ?: ''}}"
                           name="manual_button"/>
                </label>
                @if ($errors->has('manual_button'))
                    <span class="help-block">{{ $errors->first('manual_button') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</template>
<template id="proffesional_template">

</template>
<template id="questionaire_template">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h4 class="text-center">Selecteer een vragenlijst</h4>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div id="questionaire" class="form-group {{$errors->has('questionaire') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Workflow titel</span>
                    <select id="title" class="form-control" name="questionaire">
                        <option  {{ old('questionaire')=="dix" ? 'selected' : '' }} value="dix">DIX</option>
                        <option  {{ old('questionaire')=="mini_dix" ? 'selected' : '' }} value="mini_dix">MINI-DIX</option>
                        <option  {{ old('questionaire')=="healthy_scan" ? 'selected' : '' }} value="healthy_scan">Gezond leven scan</option>
                        <option  {{ old('questionaire')=="else" ? 'selected' : '' }} value="else">Anders..</option>
                    </select>
                </label>
                @if ($errors->has('questionaire'))
                    <span class="help-block">{{ $errors->first('questionaire') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div id="questionaire_else" class="row" style="display: none;">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <div id="manual_button" class="form-group {{$errors->has('questionaire_else') ? 'has-error': '' }}">
                <label class="control-label checkbox">
                    <span>Titel vragenlijst</span>
                    <input id="title" class="form-control" type="text" value="{{ old('questionaire_else') ?: ''}}"
                           name="questionaire_else"/>
                </label>
                @if ($errors->has('questionaire_else'))
                    <span class="help-block">{{ $errors->first('questionaire_else') }}</span>
                @endif
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</template>
<template id="product_template">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <h4 class="text-center">voeg product toe</h4>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="row" style="padding-top: 25px;">
        <div class="col-md-1 col-lg-1 col-xl-1"></div>
        <div class="col-md-3 col-lg-3 col-xl-3">

            <div class="form-content">
                @include('forms.default.workflow.workflow-step.template.create_product')
            </div>
        </div>
        <div class="col-md-1 col-lg-1 col-xl-1">
        </div>
        <div class="col-md-6 col-lg-6 col-xl-6">
            <div class="text-center">
                <div id="live_preview" class="content">
                    @include('forms.default.workflow.workflow-step.template.live_preview')
                </div>
            </div>
        </div>
        <div class="col-md-1 col-lg-1 col-xl-1"></div>
    </div>
</template>
<template id="questionaire_else_template">

</template>