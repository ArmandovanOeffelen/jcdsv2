<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - JCDS</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('sass/app.sass') }}" rel="stylesheet">


    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>
    {{--imports--}}
    <link rel="stylesheet" href="{{ asset('library/bootstrap3-dialog/css/bootstrap-dialog.css') }}">
    <script src="{{ asset('library/bootstrap3-dialog/js/bootstrap-dialog.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>


    @yield('scripts-head')

</head>
<body>
    <div id="app">
            {{--TODO:: Quick create in menu dynamisch maken.--}}
            @include('layouts.nav')
                <div id="page-wrapper" style="height: auto;">
                    <div class="container-fluid">
                        <!-- Page Heading -->
                        <div class="row" id="main">
                            <div class="col-sm-12 col-md-12 well" id="content">
                                @yield('content')
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
                </div>
            <!-- /#page-wrapper -->
    </div>
    @include('layouts.footer')

    @yield('templates')

    @yield('scripts')
</body>
</html>
