<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>
<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top nav-test" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            @guest
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="{{asset("storage/logo/logoJohan.png")}}"/>
                </a>
            @else
                @if(Auth::user()->isAdmin())
                    @if(isset(Auth::user()->logo_name))
                        <a class="navbar-brand" href="{{url('/admin/user-management/overview')}}">
                            <img src="{{asset("storage/logo/".Auth::user()->logo_name)}}"/>
                        </a>
                    @elseif(!isset(Auth::user()->logo_name))
                        <a class="navbar-brand" href="{{url('/admin/user-management/overview')}}">
                            <img src="{{asset("storage/logo/logoJohan.png")}}"/>
                        </a>
                    @endif
                @elseif(!Auth::user()->isAdmin())
                    @if(isset(Auth::user()->logo_name))
                        <a class="navbar-brand" href="{{url('/content/overview')}}">
                            <img src="{{asset("storage/logo/".Auth::user()->logo_name)}}"/>
                        </a>
                    @elseif(!isset(Auth::user()->logo_name))
                        <a class="navbar-brand" href="{{url('/content/overview')}}">
                            <img src="{{asset("storage/logo/logoJohan.png")}}"/>
                        </a>
                    @endif
                @endif
            @endguest
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <!-- Authentication Links -->
            @guest
                <li><a href="{{ route('login') }}">Login</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav nav-test">
                @guest
                @else
                    @if(Auth::user()->isAdmin())
                        <li>
                            <a href="{{url('admin/user-management/overview')}}" ><i class="fa fa-fw fa-users" style="color:#009de0;"></i><b> &nbsp; Bedrijven </b></a>
                        </li>

                    @elseif(!Auth::user()->isAdmin())
                        <li class="nav"><a href="{{url('content/overview')}}"><b> &nbsp;Aanleveren Content</b></a></li>
                        <li class="nav"><a href="{{route('default_controller_workflow_index')}}"><b> &nbsp;Aanleveren Workflow</b></a></li>
                    @endif

                @endguest
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
