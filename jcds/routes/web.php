<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'content','middleware' => ['auth:web']], function(){

        //Home
    Route::get('/overview', 'ContentController@index');
    Route::get('/show-form','ContentController@showForm');
    Route::post('/{company}/create-content','ContentController@create');
    Route::get('/{company}/show-update-form/{content}','ContentController@showUpdateForm');
    Route::post('/{company}/update-content/{content}','ContentController@update');


    Route::get('/workflow/overview','WorkFlowController@index')->name('default_controller_workflow_index');
    Route::get('/workflow/show-form','WorkFlowController@showForm')->name('default_show_work_flow_form');
    Route::post('/workflow/{company}/post-form', 'WorkFlowController@createMainWorkflow')->name('default_create_main_work_flow');
    Route::post('/{company}/workflow/{workFlow}/workflow-step/post-form', 'WorkFlowController@createWorkFlowStep')->name('default_create_work_flow_step');

    Route::get('/{company}/workflow/{workFlow}/show-contents','WorkFlowController@showWorkFlow')->name('default_show_work_flow');
    Route::get('/{company}/workflow/{workFlow}/show-step-form','WorkFlowController@showStepForm')->name('default_show_create_step_form');
    Route::get('/{company}/workflow/{workFlow}/step/{workFlowStep}/update/show-form','WorkFlowController@updateStepForm')->name('default_show_update_step_form_show_form');
    Route::post('/{company}/workflow/{workFlow}/step/{workFlowStep}/update','WorkFlowController@updateStep')->name('default_show_update_step_form');
    Route::post('/company/{company}/workflow/{workFlow}/update','WorkFlowController@updateFlow')->name('default_workflow_update');
    Route::get('/company/{company}/workflow/{workFlow}/show-update-form','WorkFlowController@showUpdateForm')->name('default_workflow_show_form_update');

});

Route::group(['prefix' => 'admin/ajax/template/','namespace' => 'Ajax\\Template', 'middleware' => 'auth:web'], function(){

    //Home
    Route::delete('delete/{company}/content/{content}', 'ContentController@delete');
    Route::get('/overview', 'ContentController@index');


});

Route::group(['prefix' => 'admin/ajax/template/','namespace' => 'Ajax\\Template', 'middleware' => 'auth:web','is_admin'], function(){

    //Home
    Route::put('/user/{company}/content-management/content/{content}/mark-completed', 'ContentController@markCompleted');
    Route::post('/user/{company}/content-management/content/{content}/download-image', 'ContentController@downloadImage');
    Route::delete('/user/{company}/content-management/content/{content}/delete', 'ContentController@deleteContent');
    Route::get('/user-management/{company}/content/overview', 'CompanyController@showCompany');
    Route::get('/user-management/overview', 'CompanyController@showAllCompanies');
    Route::delete('/user-management/{company}/delete', 'CompanyController@deleteCompany');


    //Categories
    Route::delete('user-management/{company}/sub-category/{subcategory}/delete', 'CategoryController@deleteSubCategory');
    Route::delete('/user-management/{company}/category/{category}/delete', 'CategoryController@deleteCategory');


    Route::get('/user-management/{company}/category/overview', 'CategoryController@showContent');

    Route::put('/user-management/{company}/credits/add-credit','CreditController@AddCredits');
    Route::put('/user-management/{company}/credits/edit-credit','CreditController@EditCredits');


    Route::delete('{company}/workflow/{workFlow}/step/{workFlowStep}/delete','WorkFlowController@delete')->name('default_show_delete_step_form');
    Route::get('{company}/workflow/{workFlow}/show-content','WorkFlowController@showWorkFlowContent')->name('default_workflow_show_content');
    Route::get('{company}/workflow/show-content','WorkFlowController@showAllWorkFlows')->name('default_workflow_default_show_content');
    Route::delete('company/{company}/workflow/{workFlow}/delete','WorkFlowController@deleteWorkFlow')->name('default_workflow_delete');
    Route::put('workflow/{workFlow}/step/{workFlowStep}/complete','WorkFlowController@markStepComplete')->name('ajax_workflow_mark_complete');
    Route::get('/company/{company}/workflow/{workFlow}/show-content','WorkFlowController@showWorkFlowForAdmin')->name('cancer');

    Route::put('/company/{company}/workflow/{workFlow}/completed','WorkFlowController@markWorkFlowComplete');



});

Route::group(['prefix' => 'admin','middleware' => ['auth:web','is_admin']], function(){


    //Bedrijven
    Route::get('/user-management/overview', 'CompanyController@index');
    Route::get('user-management/add-company/form', 'CompanyController@showForm');
    Route::post('user-management/add-company', 'CompanyController@create');
    Route::get('/user-management/{company}/category/overview','CompanyController@showCompanyCategories');
    Route::post('/user-management/{company}/edit-company','CompanyController@update');
    Route::get('/user-management/{company}/edit-company/show-form','CompanyController@showUpdateForm');
    Route::delete('/user-management/delete/{company}','CompanyController@delete');

    //Categories
    Route::get('user-management/{company}/add-category/form','CategoryController@showForm');
    Route::get('user-management/{company}/category/overview','CategoryController@index');
    Route::post('user-managemnt/{company}/category/add-category', 'CategoryController@create');


    //Content
    Route::get('/user-management/{company}/content/overview','CompanyController@showCompany');
    Route::get('/user/{company}/content-management/content/{content}/show','ContentController@showCompanyContent');

    //Workflows
    Route::get('/company/{company}/workflow/show-content','WorkFlowController@showAdminContent')->name('admin_workflow_show_content');
    Route::get('/company/{company}/workflow/{workFlow}/show-content','WorkFlowController@showWorkFlowSteps')->name('admin_workflow_step_show_content');
    Route::get('/company/{company}/workflow/{workFlow}/step/{workFlowStep}/show-content','WorkFlowController@showWorkFlowStepContent')->name('admin_workflow_step-content_show_content');




});